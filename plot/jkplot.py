import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rc
import pandas as pd
import re
import argparse

os.sys.path.append('../code/utils/')
import flowlib

def set_matplotlib(plt):
    # Set the global font to be DejaVu Sans, size 10 (or any other sans-serif font of your choice!)
#     rc('font',**{'family':'sans-serif','sans-serif':['DejaVu Sans'],'size':10})

#     # Set the font used for MathJax - more on this later
#     rc('mathtext',**{'default':'regular'})

    params = {
       'axes.labelsize': 8,
       'text.fontsize': 8,
       'legend.fontsize': 10,
       'xtick.labelsize': 10,
       'ytick.labelsize': 10,
       'text.usetex': False,
       'figure.figsize': [10., 4.] # instead of 4.5, 4.5
       }
    plt.rcParams.update(params)


class JKPlot(object):
    """
    Plot utility class for general purpose based on argparse
    """
    def __init__(self, dResult, parser, ext='npy'):
        """
        dResult : list of directories containing result files
        parser  : argparse object
        ext     : extension to extract
        """
        self.dResult = dResult
        self.parser = parser
        
        self.fList = []
        
        for res in dResult:
            self.fList += [res+f for f in os.listdir(res) if f.endswith(ext)]
            
            
    def load_result(self, **kwargs):
        """
        load one result for given parameter

        Usage:
        >>> res = load_result(opt='sgd', lr=0.05, batchSize=32, dropout=0)
        """
        len_fList = len(self.fList)
        idxRandom = np.random.randint(0, len_fList, len_fList)
        
        for i in idxRandom:
            f = self.fList[i]
            fName = os.path.basename(f)
            params = fName.split('_')
            args, temp = self.parser.parse_known_args(params)
            dic = vars(args)

            bFind = True
            for key in kwargs:
                if dic[key] != kwargs[key]:
                    bFind = False
                    break

            if bFind == True:
                print(args)
                res = np.load(f)
                return res, args, f
          
    def generate(self, **kwargs):
        """
        generate results of having parameters represented in kwargs
        """
        for f in self.fList:
            fName = os.path.basename(f)
            params = fName.split('_')
            
            args, temp = self.parser.parse_known_args(params)
            dic = vars(args)
            
            bFind = True
            for key in kwargs:
                if dic[key] != kwargs[key]:
                    bFind = False
                    break
                
            if bFind == True:
                res = np.load(f)
                dic = {}
                
                yield res, args, f