#include <stdio.h>
#include <math.h>
#include "jkimg.cuh"

__global__ void computeImageGradient(
    float* Ix, float* Iy, float* I, int nx, int ny, int nc)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;
    
    for (int c=0; c < nc; c++) {
        int idxc = idx + c*nx*ny;
        Ix[idxc] = computeGradientX(I+c*nx*ny, 0, x, y, nx, ny);
        Iy[idxc] = computeGradientY(I+c*nx*ny, 0, x, y, nx, ny);
    }

    // Ix[idx] = computeGradientX(I, 0, x, y, nx, ny);
    // Iy[idx] = computeGradientY(I, 0, x, y, nx, ny);
}

/******************************
** compute warp images of I2[x + FX[x]]
*******************************/

__global__ void warpImages(
    float* Iw, float* Ix, float* Iy, float* It, float* FX0, float* FY0,
    float* I2w, float* I2x, float* I2y,
    float* I1w, float* I1x, float* I1y, float alpha, int alphaMode,
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    float* lambd, float* weightTV,
    float lambd_const, int adaptive, float beta, float param1,
    int nx, int ny, int nc)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;

    // printf("x:%d, y:%d, idx:%d ", x, y, idx);

    // initialization
    GX[idx] = FX[idx];
    GY[idx] = FY[idx];

    FX0[idx] = FX[idx];
    FY0[idx] = FY[idx];

    // compute warping (I2w, I2x, I2y)
    // Iw[idx] = 0;
    // Ix[idx] = 0;
    // Iy[idx] = 0;
    // It[idx] = 0;

    float Iw1 = 0, It_mean=0;

    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c * nx * ny;
        float Iw_val, w1, w2;

        if (alphaMode == 0)
        {
            w1 = 1;
            w2 = 0;
        }
        else if (alphaMode == 1)
        {
            w1 = alpha;
            w2 = -(1-alpha);
        }
        else if (alphaMode == 2)
        {
            w1 = 1;
            w2 = -alpha;
        }
        else if (alphaMode == 3)
        {
            w1 = alpha;
            w2 = -(alpha-1);
        }
        
        I2w[idxc] = interp2_bicubic_at(I2+c*nx*ny, x+w1*FX[idx], y+w1*FY[idx], nx, ny, I1[idxc]);
        I1w[idxc] = interp2_bicubic_at(I1+c*nx*ny, x+w2*FX[idx], y+w2*FY[idx], nx, ny, I2[idxc]);
        
        It[idxc] = I2w[idxc] - I1w[idxc];
        It_mean += It[idxc];
    }
    
    It_mean /= (float) nc;
    
    // update Lambda
    // constant
    if (adaptive == 0)
    {
        lambd[idx] = lambd_const;
        weightTV[idx] = 1. - lambd_const;
    } else if (adaptive == -1)
    {
        lambd[idx] = lambd_const;
        weightTV[idx] = 1.;
    } else if (adaptive == -2)
    {
        lambd[idx] = 1.;
        weightTV[idx] = lambd_const;
    }
    else
    {
        float dLambda   = exp(- fabs(It_mean) / beta) - 0.01;

        // adaptive (simple)
        if (adaptive == 1)
        {
            float dLambda   = exp(- fabs(It_mean) / beta) - param1;
            lambd[idx]      = dLambda;
            weightTV[idx]   = 1. - dLambda;
            // if (x ==100 && y == 100) printf("lambd:%f,weightTV:%f,",lambd[idx], weightTV[idx]);
        }
        // adaptive (reserved. we can employ param1 value)
        else if (adaptive == 2)
        {
            lambd[idx]      = 1.;
            weightTV[idx]   = param1 * (1. - dLambda);
            //weightTV[idx]   = param1 * (1. - lambd_const);
        }
        else if (adaptive == 3)
        {
            
            lambd[idx]      = lambd_const;
            weightTV[idx]   = (1. - dLambda) + param1;
            
            // if (x == 0 && y ==0) printf("a3,%f", weightTV[idx]);
            //weightTV[idx]   = param1 * (1. - lambd_const);
        }
        else if (adaptive == 100) {
            lambd[idx] = dLambda;
            weightTV[idx] = 1- lambd[idx];
            // error
        }
    }
}

__global__ void updateIxIy(
    float* Iw, float* Ix, float* Iy, float* It, float* FX0, float* FY0,
    float* I2w, float* I2x, float* I2y,
    float* I1w, float* I1x, float* I1y, float alpha, int alphaMode,
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    float* lambd, float* weightTV,
    float lambd_const, int adaptive, float beta, float param1,
    int nx, int ny, int nc)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;

    // initialization
    GX[idx] = FX[idx];
    GY[idx] = FY[idx];

    FX0[idx] = FX[idx];
    FY0[idx] = FY[idx];
    
    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c*nx*ny;
        
        float w1, w2;

        if (alphaMode == 0)
        {
            w1 = 1;
            w2 = 0;
        }
        else if (alphaMode == 1)
        {
            w1 = alpha;
            w2 = -(1-alpha);
        }
        else if (alphaMode == 2)
        {
            w1 = 1;
            w2 = -alpha;
        }
        else if (alphaMode == 3)
        {
            w1 = alpha;
            w2 = -(alpha-1);
        }
        
        // if (alphaMode == 0)
        // {
        //     Ix[idxc] = alpha*I2x[idxc] + (1-alpha)*I1x[idxc];
        //     Iy[idxc] = alpha*I2y[idxc] + (1-alpha)*I1y[idxc];
        // }
        // else if (alphaMode == 1)
        // {
        //     Ix[idxc] = I2x[idxc] + (1-alpha)/alpha*I1x[idxc];
        //     Iy[idxc] = I2y[idxc] + (1-alpha)/alpha*I1y[idxc];
        // }
        // else if (alphaMode == 2)
        // {
        //     Ix[idxc] = I2x[idxc] + (alpha)*I1x[idxc];
        //     Iy[idxc] = I2y[idxc] + (alpha)*I1y[idxc];
        // }
        
        Ix[idxc] = w1*I2x[idxc] - w2*I1x[idxc];
        Iy[idxc] = w1*I2y[idxc] - w2*I1y[idxc];
    }
}

/******************************
** compute energy
*******************************/
__global__ void computeEnergy (
    float* I1, float* I2, float* FX, float* FY, float* FX0, float* FY0,
    float* GX, float* GY, float* HX, float* HY, float* P, float* Q, float* R,
    float* PX, float* PY, float* QX, float* QY, float* PHI, float* PHIX, float* PHIY,
    float* Iw, float* Ix, float* Iy, float* It, float* S,
    float* LHS, float* RHS1, float* RHS2, float* LHS_PHI, float* RHS_PHI,
    float* lambd, float* weightTV, float* energy,
    float dHuberData, float dHuberRegular, float dWeightSplit, float dHuberLight, float dWeightLight,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }
    int idx = x + y * nx;

    /******************************
    ** data fidelity energy
    ******************************/
    float dVal, dR;
    float dEnergy = 0;
    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c * nx * ny;
        dVal = It[idxc] + Ix[idxc] * (FX[idx] - FX0[idx]) \
                + Iy[idxc] * (FY[idx] - FY0[idx]) - S[idx];
        R[idxc] = dR;

        dEnergy += lambd[idx] * (fabs(dR) + (dVal-dR)*(dVal-dR) / (2.*dHuberData) );
        dEnergy *= 0.1;
        // dEnergy += fabs(dR)
    }

    /******************************
    ** compute gradient of GX, GY
    ******************************/
    float gxx, gxy, gyx, gyy;
    gxx = computeGradientX(GX, 1, x, y, nx, ny);
    gxy = computeGradientY(GX, 1, x, y, nx, ny);
    gyx = computeGradientX(GY, 1, x, y, nx, ny);
    gyy = computeGradientY(GY, 1, x, y, nx, ny);
    
    
    /******************************
    ** compute Energy
    ******************************/
    float term1 = fabs(PX[idx]) + fabs(PY[idx]) + fabs(QX[idx]) + fabs(QY[idx]);
    float sq11 = gxx-PX[idx];
    float sq12 = gxy-PY[idx];
    float sq21 = gyx-QX[idx];
    float sq22 = gyy-QY[idx];
    dEnergy += weightTV[idx] * ( term1 + \
                (sq11*sq11+sq12*sq12+sq21*sq21+sq22*sq22) / (2.*dHuberRegular)   );

    sq11 = FX[idx] - GX[idx] + HX[idx];
    sq12 = FY[idx] - GY[idx] + HY[idx];
    dEnergy += dWeightSplit * (sq11*sq11 + sq12*sq12) / 2.;

    energy[idx] = dEnergy;
}

/******************************
** inner iteration of Huber-Huber model
*******************************/
__global__ void computeFlow(
    float* I1, float* I2, float* FX, float* FY, float* FX0, float* FY0,
    float* GX, float* GY, float* HX, float* HY, float* P, float* Q, float* R,
    float* PX, float* PY, float* QX, float* QY, float* PHI, float* PHIX, float* PHIY,
    float* Iw, float* Ix, float* Iy, float* It, float* S,
    float* LHS, float* RHS1, float* RHS2, float* LHS_PHI, float* RHS_PHI,
    float* lambd, float* weightTV, float* energy,
    float dHuberData, float dHuberRegular, float dWeightSplit, float dHuberLight, float dWeightLight,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }
    int idx = x + y * nx;

    /******************************
    ** update R
    ******************************/
    float dVal, dR;
    float dEnergy = 0;
    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c * nx * ny;
        dVal = It[idxc] + Ix[idxc] * (FX[idx] - FX0[idx]) \
                + Iy[idxc] * (FY[idx] - FY0[idx]) - S[idx];
        dR = shrinkageScalar(dVal, dHuberData);
        R[idxc] = dR;

        dEnergy += lambd[idx] * (fabs(dR) + (dVal-dR)*(dVal-dR) / (2.*dHuberData) );
        dEnergy *= 0.1;
        // dEnergy += fabs(dR)
    }

    /******************************
    ** compute gradient of GX, GY
    ******************************/
    float gxx, gxy, gyx, gyy;
    gxx = computeGradientX(GX, 1, x, y, nx, ny);
    gxy = computeGradientY(GX, 1, x, y, nx, ny);
    gyx = computeGradientX(GY, 1, x, y, nx, ny);
    gyy = computeGradientY(GY, 1, x, y, nx, ny);

    /******************************
    ** update P, Q
    ******************************/
    PX[idx] = shrinkageScalar(gxx, dHuberRegular);
    PY[idx] = shrinkageScalar(gxy, dHuberRegular);
    QX[idx] = shrinkageScalar(gyx, dHuberRegular);
    QY[idx] = shrinkageScalar(gyy, dHuberRegular);

    /******************************
    ** update HX, HY
    ******************************/
    HX[idx] += FX[idx] - GX[idx];
    HY[idx] += FY[idx] - GY[idx];

    /******************************
    ** update FX, FY
    ******************************/

    float dPara = dHuberData*dWeightSplit;
    float lx, ly, numerx, numery, denom;
    float dIxIx=0, dIyIy=0, dIxIy=0, dIxR=0, dIyR=0;
    float dItIx=0, dItIy=0;
    
    
    // nc = 3;
    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c*nx*ny;
        float dR = R[idxc];
        float dIt = It[idxc];
        
        dIxIx += Ix[idxc]*Ix[idxc];
        dIxIy += Ix[idxc]*Iy[idxc];
        dIyIy += Iy[idxc]*Iy[idxc];
        dItIx += dIt*Ix[idxc];
        dItIy += dIt*Iy[idxc];
        
        dIxR += Ix[idxc]*(dR + S[idx]);
        dIyR += Iy[idxc]*(dR + S[idx]);
    }
    
    dIxIx = dIxIx*lambd[idx] / (float)nc;
    dIxIy *= (lambd[idx] / (float) nc);
    dIyIy *= (lambd[idx] / (float) nc);
    dItIx *= (lambd[idx] / (float) nc);
    dItIy *= (lambd[idx] / (float) nc);
    
    dIxR *= (lambd[idx] / (float) nc);
    dIyR *= (lambd[idx] / (float) nc);

    lx = -dItIx + dIxR + dPara*(GX[idx]-HX[idx]) \
            + dIxIx*FX0[idx] + dIxIy*FY0[idx];
    ly = -dItIy + dIyR + dPara*(GY[idx]-HY[idx]) \
            + dIxIy*FX0[idx] + dIyIy*FY0[idx];

    numerx = (dPara + dIyIy) * lx - dIxIy*ly;
    numery = (dPara + dIxIx) * ly - dIxIy*lx;
    
    // numerx = 1;
    // numery = 1;

    denom = (dPara + dIxIx) * (dPara + dIyIy) - dIxIy*dIxIy;
    
    // printf("numerx: %f, numery: %f, denom: %f\n", numerx, numery, denom);

    FX[idx] = numerx / (denom);
    FY[idx] = numery / (denom);

//     __syncthreads();

//     /******************************
//     ** update GX GY
//     ******************************/
//     float p_div, q_div;
//     p_div = computeDivergence(PX, PY, x, y, nx, ny);
//     q_div = computeDivergence(QX, QY, x, y, nx, ny);

//     LHS[idx]  = weightTV[idx] / (dHuberRegular * dWeightSplit);
//     RHS1[idx] = FX[idx] + HX[idx] - LHS[idx] * p_div;
//     RHS2[idx] = FY[idx] + HY[idx] - LHS[idx] * q_div;

//     // Gauss-Seidel
    
    /******************************
    ** compute Energy
    ******************************/
//     if (bComputeEnergy == 1)
//     {
//         float term1 = fabs(PX[idx]) + fabs(PY[idx]) + fabs(QX[idx]) + fabs(QY[idx]);
//         float sq11 = gxx-PX[idx];
//         float sq12 = gxy-PY[idx];
//         float sq21 = gyx-QX[idx];
//         float sq22 = gyy-QY[idx];
//         dEnergy += weightTV[idx] * ( term1 + \
//                     (sq11*sq11+sq12*sq12+sq21*sq21+sq22*sq22) / (2.*dHuberRegular)   );

//         sq11 = FX[idx] - GX[idx] + HX[idx];
//         sq12 = FY[idx] - GY[idx] + HY[idx];
//         dEnergy += dWeightSplit * (sq11*sq11 + sq12*sq12) / 2.;

//         energy[idx] = dEnergy;
//     }

//     /******************************
//     ** illumination term
//     ******************************/
//     // if weight of illumination term is small, ignore this term
    if (dWeightLight <= 1e-3)
        return;

    float sx, sy, phi_div;

    sx = computeGradientX(S, 1, x, y, nx, ny);
    sy = computeGradientY(S, 1, x, y, nx, ny);

    PHIX[idx] = shrinkageScalar(sx, dHuberLight);
    PHIY[idx] = shrinkageScalar(sy, dHuberLight);

//     phi_div = computeDivergence(PHIX, PHIY, x, y, nx, ny);

//     // LHS_PHI can be updated when warping
//     LHS_PHI[idx] = dWeightLight * dHuberData / (dHuberLight * lambd[idx]);
//     RHS_PHI[idx] = LHS_PHI[idx] * -phi_div + It[idx] + Ix[idx] * (FX[idx]-FX0[idx]) \
//                     + Iy[idx] * (FY[idx] - FY0[idx]) - dR;

//     // Gauss-Seidel
}

__global__ void updateGXGY(
    float* I1, float* I2, float* FX, float* FY, float* FX0, float* FY0,
    float* GX, float* GY, float* HX, float* HY, float* P, float* Q, float* R,
    float* PX, float* PY, float* QX, float* QY, float* PHI, float* PHIX, float* PHIY,
    float* Iw, float* Ix, float* Iy, float* It, float* S,
    float* LHS, float* RHS1, float* RHS2, float* LHS_PHI, float* RHS_PHI,
    float* lambd, float* weightTV,
    float dHuberData, float dHuberRegular, float dWeightSplit, float dHuberLight, float dWeightLight,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }
    int idx = x + y * nx;

    /******************************
    ** update GX GY
    ******************************/
    float p_div, q_div;
    p_div = computeDivergence(PX, PY, x, y, nx, ny);
    q_div = computeDivergence(QX, QY, x, y, nx, ny);

    LHS[idx]  = weightTV[idx] / (dHuberRegular * dWeightSplit);
    RHS1[idx] = FX[idx] + HX[idx] - LHS[idx] * p_div;
    RHS2[idx] = FY[idx] + HY[idx] - LHS[idx] * q_div;

    // Gauss-Seidel

    /******************************
    ** illumination term
    ******************************/
    // if weight of illumination term is small, ignore this term
    if (dWeightLight <= 1e-3)
        return;

    float sx, sy, phi_div;

//     sx = computeGradientX(S, 1, x, y, nx, ny);
//     sy = computeGradientY(S, 1, x, y, nx, ny);

//     PHIX[idx] = shrinkageScalar(sx, dHuberLight);
//     PHIY[idx] = shrinkageScalar(sy, dHuberLight);

    phi_div = computeDivergence(PHIX, PHIY, x, y, nx, ny);

    if (dWeightLight <= 1e-3)
        return;
    
    // LHS_PHI can be updated when warping
    LHS_PHI[idx] = dWeightLight * dHuberData / (dHuberLight * lambd[idx]);
    
    float dIt=0, dIx=0, dIy=0, dR=0;
    
    for (int c=0; c < nc; c++) {
        int idxc = idx + c*nx*ny;
        dIt += It[idxc];
        dIx += Ix[idxc];
        dIy += Iy[idxc];
        dR += R[idxc];
    }
    
    dIt /= (float) nc;
    dIx /= (float) nc;
    dIy /= (float) nc;
    dR /= (float) nc;
    
    RHS_PHI[idx] = LHS_PHI[idx] * -phi_div + dIt + dIx * (FX[idx]-FX0[idx]) \
                    + dIy * (FY[idx] - FY0[idx]) - dR;

    // Gauss-Seidel
}

__global__ void doGaussSeidel(
    float* GX, float* GY, float*LHS, float* RHS1, float* RHS2, 
    float* S, float* LHS_PHI, float* RHS_PHI, float dWeightLight, int nx, int ny)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }
    /******************************
    ** for GX, GY
    ******************************/
    computeGaussSeidel(GX, LHS, RHS1, x, y, nx, ny);
    computeGaussSeidel(GY, LHS, RHS2, x, y, nx, ny);

    if (dWeightLight <= 1e-3)
        return;
    /******************************
    ** for illumination term
    ******************************/
    computeGaussSeidel(S, LHS_PHI, RHS_PHI, x, y, nx, ny);
}
