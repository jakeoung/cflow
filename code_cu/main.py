##############################
# Import necessary modules
##############################
import cffi
from utils import flowlib

import numpy as np
import os
import time
import math

from flow_hh_wrapper import *

from PIL import Image
from scipy import misc
from scipy import ndimage
from scipy import interpolate

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.set_defaults()
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default=100, help='iteration number')
parser.add_argument('-n2', '--niterinner', type=int, default=30, help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default=1, help='number of scales in pyramid')

parser.add_argument('--alpha', type=float, default=1.0, help='weight of alpha | I2(x+alpha w) - I1(x-(1-alpha)w)| (default: 1)')
parser.add_argument('--alphaMode', type=int, default=0, help='alpha mode: 0, 1, 2')
parser.add_argument('--alphaRatio', type=float, default=1.05, help='update ratio of alpha (default: 1.05)')

parser.add_argument('-a','--adaptive', type=int, default=0, help='adaptive method: 0,1,2')
parser.add_argument('--lambd', type=float, default=0.91, help='constant lambda')
parser.add_argument('--dBeta', type=float, default=1.0, help='beta in adaptive')
parser.add_argument('--param1', type=float, default=0.02, help='reserved param1')

parser.add_argument('--dHuberData', type=float, default=0.01)
parser.add_argument('--dHuberRegular', type=float, default=0.5)
parser.add_argument('-s', '--dWeightSplit', type=float, default=0.1, help='step size (small value is large step)')

parser.add_argument('--dWeightLight', type=float, default=20.0001)
parser.add_argument('-v', '--verbose', action='count', default=0, help='verbose mode')

args, unparsed = parser.parse_known_args()
fResult = time.strftime('%m%d_')

dic = vars(args)
keylist = list(dic.keys())
keylist.sort()

for key in keylist:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser.add_argument('--dHuberLight', type=float, default=0.3)
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')

parser.add_argument('--root', type=str, default='../', help='work folder')
parser.add_argument('--dResult', type=str, default='../result/ours/')

args = parser.parse_args(namespace=args)
fResult_ = os.path.join(args.dResult, fResult)

args.root = os.path.abspath(args.root)
print(args)
# print(fResult_)

##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon', 'Grove2', 'Grove3', 'Hydrangea',
            'RubberWhale', 'Urban2', 'Urban3', 'Venus']

dir_data = 'data/other-data-two/'

im1ori = misc.imread(os.path.join(args.root, dir_data + seq_list[args.seq] + '/frame10.png'))

im2ori = misc.imread(os.path.join(args.root, dir_data + seq_list[args.seq] + '/frame11.png'))

# pre-smothing

gt = flowlib.read_flow(os.path.join(args.root,
                                    'data/other-gt-flow/' + seq_list[args.seq] + '/flow10.flo'))

# gt.astype(dtype=np.float32, copy=False)
gt1 = gt[:, :, 0].astype(dtype=np.float32)
gt2 = gt[:, :, 1].astype(dtype=np.float32)

nRow, nCol, nCha = np.atleast_3d(im1ori).shape

# zoom_factor = 0.5;
nRows = math.ceil(nRow / math.pow(2, (args.pyramid -1)) )
nCols = math.ceil(nCol / math.pow(2, (args.pyramid -1)) )

FX = np.zeros([nRows, nCols], dtype=np.float32)
FY = np.zeros([nRows, nCols], dtype=np.float32)

args.bGT = 0
args.gt = gt

flow_wrapper = FlowWrapper(args.root)

alpha = args.alpha
niter = args.niter

##############################
# compute flow, using pyraimd
##############################
for s in range(args.pyramid):
    if s + 1 == args.pyramid:
        I1s = im1ori
        I2s = im2ori
    else:
        I1s = im1ori.copy()
        I2s = im2ori.copy()
        # sigma = 0.2
        # I1s = ndimage.gaussian_filter(im1ori, sigma)
        # I2s = ndimage.gaussian_filter(im2ori, sigma)
    
        I1s = misc.imresize(I1s, 1 / math.pow(2, args.pyramid-s-1), 'cubic')
        I2s = misc.imresize(I2s, 1 / math.pow(2, args.pyramid-s-1), 'cubic')


    im1 = np.atleast_3d(I1s)
    im2 = np.atleast_3d(I2s)

    im1 = im1.transpose(2, 0, 1)
    im2 = im2.transpose(2, 0, 1)

    im1 = np.ascontiguousarray(im1, np.float32)
    im2 = np.ascontiguousarray(im2, np.float32)

    min_val = min(im1.min(), im2.min())
    max_val = max(im1.max(), im2.max())

    im1 = 1.*(im1 - min_val) / (max_val - min_val)
    im2 = 1.*(im2 - min_val) / (max_val - min_val)

    if s+1 == args.pyramid:
        args.alpha = alpha
        args.niter = niter
        args.bGT=1
    else:
        args.alpha = 1
        args.niter = (int)( niter *  1. / math.pow(2, (args.pyramid -s-1)) )
        print('niter: ', args.niter)
    
    print('@ max(FX): %f' %(FX.max()))
    print(im1.shape, im2.shape)


    print('@ optimize for scale: %f' % (1. / math.pow(2, (args.pyramid -s-1))))

    FX, FY = flow_wrapper.run(im1, im2, FX, FY, args)

    print('@ done. max(FX): %f' %(FX.max()))

    print('@ before zooming flows. shape: ', FX.shape)

    if s+1 == args.pyramid:
        break

    nRows = math.ceil(nRow / math.pow(2, (args.pyramid - s-2)) )
    nCols = math.ceil(nCol / math.pow(2, (args.pyramid - s-2)) )
    ratio = 2.

    FX = ndimage.median_filter(FX, [3, 3]);
    FY = ndimage.median_filter(FY, [3, 3]);
    
    FX = ratio * ndimage.zoom(FX, 2, order=4, mode='nearest') # nearest
    FY = ratio * ndimage.zoom(FY, 2, order=4, mode='nearest')
    # FX = ratio * misc.imresize(FX, [nRows, nCols], interp='nearest')
    # FY = ratio * misc.imresize(FY, [nRows, nCols], interp='nearest')

    print('@ after zooming flows. shape: ', FX.shape)
    print('@ after FX interpolation. max(FX): %f' %(FX.max()))


    FX.astype(dtype=np.float32, copy=False)
    FY.astype(dtype=np.float32, copy=False)

# compute_gradient
# print(FX)

##############################
# write output
##############################
ape, aae = flowlib.flow_error(gt[:, :, 0], gt[:, :, 1], FX, FY)
print('APE: %f, AAE: %f, max_FX: %f' % (ape, aae, FX.max()))

flow_img = flowlib.flow_to_image(FX, FY)

if args.verbose:
    im = Image.fromarray(flow_img).show()

if args.verbose >= 2:
    np.save(fResult_, [FX, FY, ape, aae, flow_wrapper.log])
    
elif os.path.exists(args.dResult) and ape <= 10:
    np.save(fResult_, [FX, FY, ape, aae])
