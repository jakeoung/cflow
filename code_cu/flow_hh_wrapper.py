#############################
# pycuda
##############################
import pycuda.driver as drv
import pycuda.gpuarray as pc
import pycuda.autoinit
from pycuda.compiler import SourceModule

from utils import *
import os
import numpy as np
import math


class FlowWrapper(object):
    def __init__(self, work_path):
        # read cuda functions

        cuda_code = ''
        with open(os.path.join(work_path, 'code_cu/src/flow_hh.c'), 'r') as f:
            cuda_code += f.read()

        include_dirs = [os.path.join(work_path, 'code_cu/src/')]

        mod = SourceModule(cuda_code,
                           include_dirs=[os.path.join(work_path, 'code_cu/src/')])

        self.computeImageGradient = mod.get_function("computeImageGradient")
        self.computeFlow = mod.get_function("computeFlow")
        self.doGaussSeidel = mod.get_function("doGaussSeidel")
        # self.warp = mod.get_function("warp")
        self.updateGXGY = mod.get_function("updateGXGY")
        self.updateIxIy = mod.get_function("updateIxIy")
        self.warpImages = mod.get_function("warpImages")
        
        self.computeEnergy = mod.get_function("computeEnergy")
        
        self.log = {}
        self.log['energy'] = []
        self.log['ape'] = []
        self.log['aae'] = []

    def run(self, I1, I2, FX, FY, args):
        """
        :param I1: numpy array [C x H x W]
        :param I2: numpy array [C x H x W]
        :param FX: Flow field of x direction
        :param FY: Flow field of y direction
        :param args: arguments

        :return: update FX, FY
        """
        nCha, nRow, nCol = I1.shape

        if args.verbose:
            print('@ nCha:', nCha, ', nRow:', nRow, ', nCol:', nCol, ', max(FX):', FX.max())

        I1_gpu = drv.mem_alloc(I1.size * I1.dtype.itemsize)
        I2_gpu = drv.mem_alloc(I2.size * I2.dtype.itemsize)

        FX_gpu = drv.mem_alloc(FX.size * FX.dtype.itemsize)
        FY_gpu = drv.mem_alloc(FY.size * FY.dtype.itemsize)

        FX0_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        FY0_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

        GX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        GY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

        I2w_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        I2x_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        I2y_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)

        I1w_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        I1x_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        I1y_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)

        # normalized warping values
        # It_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        # Iw_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        # Ix_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        # Iy_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        
        It_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        Iw_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        Ix_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        Iy_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        
        # Iw1_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        # Ix1_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        # Iy1_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
        
        R_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)

        S_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        P_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        Q_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        
        HX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        HY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        PX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        PY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        QX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        QY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        
        energy = np.zeros([nRow, nCol], dtype=np.float32)
        energy_gpu = drv.mem_alloc(energy.size * energy.dtype.itemsize)
        # energy_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        # drv.memcpy_htod(energy_gpu, energy)

        # for GS sweep of GX, GY
        LHS_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        RHS1_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        RHS2_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

        # adaptive lambda
        lambd_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        weightTV_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

        # for illumination term
        PHI_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        PHIX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        PHIY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        LHS_PHI_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
        RHS_PHI_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

        drv.memcpy_htod(I1_gpu, I1)
        drv.memcpy_htod(I2_gpu, I2)
        drv.memcpy_htod(FX_gpu, FX)
        drv.memcpy_htod(FY_gpu, FY)

        nRow = np.int32(nRow)
        nCol = np.int32(nCol)
        nCha = np.int32(nCha)
        nIterGS = np.int32(10)

        dHuberData = np.float32(args.dHuberData)
        dHuberRegular = np.float32(args.dHuberRegular)
        dWeightSplit = np.float32(args.dWeightSplit)
        dHuberLight = np.float32(args.dHuberLight)
        dWeightLight = np.float32(args.dWeightLight)

        lambd_const = np.float32(args.lambd)
        adaptive = np.int32(args.adaptive)
        beta = np.float32(args.dBeta)
        param1 = np.float32(args.param1)
        alpha = np.float32(args.alpha)
        alphaMode = np.int32(args.alphaMode)

        blockSize = (32, 28, 1)
        gridSize = ((int)((nCol) / blockSize[0] + 1), (int)((nRow + 2) / blockSize[1]) + 1)
        if args.verbose:
            print('@ blockSize:', blockSize, ', gridSize:', gridSize)

        assert (blockSize[0] * gridSize[0] >= nCol)
        
        if alphaMode == 0:
            alpha = np.float32(1)

        # self.computeImageGradient(I2x_gpu, I2y_gpu, I2_gpu, nCol, nRow, nCha,
        #                           block=blockSize, grid=gridSize);
        # self.computeImageGradient(I1x_gpu, I1y_gpu, I1_gpu, nCol, nRow, nCha,
        #                           block=blockSize, grid=gridSize);
        
        for it in range(args.niter):
            if args.verbose >= 2 and args.bGT:
                self.computeEnergy(I1_gpu, I2_gpu, FX_gpu, FY_gpu, FX0_gpu, FY0_gpu,
                 GX_gpu, GY_gpu, HX_gpu, HY_gpu, P_gpu, Q_gpu, R_gpu,
                 PX_gpu, PY_gpu, QX_gpu, QY_gpu, PHI_gpu, PHIX_gpu, PHIY_gpu,
                 Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, S_gpu,
                 LHS_gpu, RHS1_gpu, RHS2_gpu, LHS_PHI_gpu, RHS_PHI_gpu,
                 lambd_gpu, weightTV_gpu, energy_gpu,
                 dHuberData, dHuberRegular, dWeightSplit, dHuberLight, dWeightLight,
                 nCol, nRow, nCha,
                 block=blockSize, grid=gridSize
                 )
                
                #len(self.log['energy']) <= 1000:
                drv.memcpy_dtoh(energy, energy_gpu);
                energy_mean = np.mean(energy);
                # print(energy_mean)

                drv.memcpy_dtoh(FX, FX_gpu)
                drv.memcpy_dtoh(FY, FY_gpu)
                ape, aae = flowlib.flow_error(args.gt[:, :, 0], args.gt[:, :, 1], FX, FY)
                # print(energy_mean, ape, aae)
                self.log['energy'].append(energy_mean)
                self.log['ape'].append(ape)
                self.log['aae'].append(aae)
            
            
            # if it >= 800:
            #     dWeightSplit = np.float32(args.dWeightSplit*2.)
                
            # if args.alphaMode == 2:
            #     alpha = np.float32(args.alpha)
            
            if it % 3 == 0 and it > 0 and args.bGT and args.alphaMode:
                if alphaMode == 3 and alpha != 1:
                    alpha = np.float32(max(alpha*args.alphaRatio, 1.0))
                    print('alpha: ', alpha)
                elif alphaMode < 3 and alpha != 1:
                    alpha = np.float32(min(alpha*args.alphaRatio, 1.0))
                    
                    if alpha < 1e-1:
                        alpha = np.float32(0)
                    else:
                        print('alpha: ', alpha)
                
            self.warpImages(Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, FX0_gpu, FY0_gpu,
                      I2w_gpu, I2x_gpu, I2y_gpu,
                      I1w_gpu, I1x_gpu, I1y_gpu, alpha, alphaMode,
                      I1_gpu, I2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
                      lambd_gpu, weightTV_gpu, lambd_const, adaptive, beta, param1,
                      nCol, nRow, nCha,
                      block=blockSize, grid=gridSize)

            self.computeImageGradient(I2x_gpu, I2y_gpu, I2w_gpu, nCol, nRow, nCha,
                                  block=blockSize, grid=gridSize);
            self.computeImageGradient(I1x_gpu, I1y_gpu, I1w_gpu, nCol, nRow, nCha,
                                  block=blockSize, grid=gridSize);
            
            self.updateIxIy(Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, FX0_gpu, FY0_gpu,
                      I2w_gpu, I2x_gpu, I2y_gpu,
                      I1w_gpu, I1x_gpu, I1y_gpu, alpha, alphaMode,
                      I1_gpu, I2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
                      lambd_gpu, weightTV_gpu, lambd_const, adaptive, beta, param1,
                      nCol, nRow, nCha,
                      block=blockSize, grid=gridSize)
                        
            for j in range(args.niterinner):                    
                self.computeFlow(I1_gpu, I2_gpu, FX_gpu, FY_gpu, FX0_gpu, FY0_gpu,
                                 GX_gpu, GY_gpu, HX_gpu, HY_gpu, P_gpu, Q_gpu, R_gpu,
                                 PX_gpu, PY_gpu, QX_gpu, QY_gpu, PHI_gpu, PHIX_gpu, PHIY_gpu,
                                 Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, S_gpu,
                                 LHS_gpu, RHS1_gpu, RHS2_gpu, LHS_PHI_gpu, RHS_PHI_gpu,
                                 lambd_gpu, weightTV_gpu, energy_gpu,
                                 dHuberData, dHuberRegular, dWeightSplit, dHuberLight, dWeightLight,
                                 nCol, nRow, nCha,
                                 block=blockSize, grid=gridSize
                                 )
                
                # if args.verbose >= 2 and args.bGT and args.alphaMode==2:
                #     alpha = np.float32(min(alpha*args.alphaRatio, 1.0))
                #     if alpha < 1e-2:
                #         alpha = np.float32(0)
                        
                    # if j % 5 == 0:
                    #     print('alpha: ', alpha)    
                    
                self.updateGXGY(I1_gpu, I2_gpu, FX_gpu, FY_gpu, FX0_gpu, FY0_gpu,
                                GX_gpu, GY_gpu, HX_gpu, HY_gpu, P_gpu, Q_gpu, R_gpu,
                                PX_gpu, PY_gpu, QX_gpu, QY_gpu, PHI_gpu, PHIX_gpu, PHIY_gpu,
                                Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, S_gpu,
                                LHS_gpu, RHS1_gpu, RHS2_gpu, LHS_PHI_gpu, RHS_PHI_gpu,
                                lambd_gpu, weightTV_gpu,
                                dHuberData, dHuberRegular, dWeightSplit, dHuberLight, dWeightLight,
                                nCol, nRow, nCha,
                                block=blockSize, grid=gridSize)

                for k in range(nIterGS):
                    self.doGaussSeidel(GX_gpu, GY_gpu, LHS_gpu, RHS1_gpu, RHS2_gpu,
                                       S_gpu, LHS_PHI_gpu, RHS_PHI_gpu, dWeightLight,
                                       nCol, nRow,
                                       block=blockSize, grid=gridSize)
                    
                # if args.verbose >= 2 and args.bGT:
                #     #len(self.log['energy']) <= 1000:
                #     drv.memcpy_dtoh(energy, energy_gpu);
                #     energy_mean = np.mean(energy);
                #     print(energy[30,30])
                    
            
                
            # lambd = lambd_gpu.get()
            # print('max(lambd): %f, min(lambd): %f, mean: %f, var: %f' % (lambd.max(), lambd.min(), lambd.mean(), lambd.var()))
            if it % 50 == 0:
                drv.memcpy_dtoh(FX, FX_gpu)
                drv.memcpy_dtoh(FY, FY_gpu)

                # if (alpha >= 0.5 and alpha < 0.99999):
                #     FX += (1.-alpha)*FX
                #     FY += (1.-alpha)*FY
                # elif (alpha < 0.5):
                #     FX += (1.-alpha)*FX
                #     FY += (1.-alpha)*FY

                max_fx = FX.max()
                assert (max_fx <= 100 and max_fx >= -100)
                
                if args.bGT:
                    ape, aae = flowlib.flow_error(args.gt[:, :, 0], args.gt[:, :, 1], FX, FY)
                    print('iter: %4d, APE: %f, AAE: %f, max_FX: %f' % (it, ape, aae, FX.max()))
                    if args.adaptive and args.verbose:
                        lambd = lambd_gpu.get()
                        weightTV = weightTV_gpu.get()
                        print('lambd,    max: %f, min: %f, mean: %f, var: %f' % (
                        lambd.max(), lambd.min(), lambd.mean(), lambd.var()))
                        print('weightTV, max: %f, min: %f, mean: %f, var: %f' % (
                        weightTV.max(), weightTV.min(), weightTV.mean(), weightTV.var()))

        if args.verbose >= 2 and args.bGT:
            self.computeEnergy(I1_gpu, I2_gpu, FX_gpu, FY_gpu, FX0_gpu, FY0_gpu,
             GX_gpu, GY_gpu, HX_gpu, HY_gpu, P_gpu, Q_gpu, R_gpu,
             PX_gpu, PY_gpu, QX_gpu, QY_gpu, PHI_gpu, PHIX_gpu, PHIY_gpu,
             Iw_gpu, Ix_gpu, Iy_gpu, It_gpu, S_gpu,
             LHS_gpu, RHS1_gpu, RHS2_gpu, LHS_PHI_gpu, RHS_PHI_gpu,
             lambd_gpu, weightTV_gpu, energy_gpu,
             dHuberData, dHuberRegular, dWeightSplit, dHuberLight, dWeightLight,
             nCol, nRow, nCha,
             block=blockSize, grid=gridSize
             )

            #len(self.log['energy']) <= 1000:
            drv.memcpy_dtoh(energy, energy_gpu);
            energy_mean = np.mean(energy);
            # print(energy_mean)

            drv.memcpy_dtoh(FX, FX_gpu)
            drv.memcpy_dtoh(FY, FY_gpu)
            ape, aae = flowlib.flow_error(args.gt[:, :, 0], args.gt[:, :, 1], FX, FY)
            # print(energy_mean, ape, aae)
            self.log['energy'].append(energy_mean)
            self.log['ape'].append(ape)
            self.log['aae'].append(aae)

                        
        drv.memcpy_dtoh(FX, FX_gpu)
        drv.memcpy_dtoh(FY, FY_gpu)

        return FX, FY
