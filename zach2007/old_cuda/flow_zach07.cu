#include <stdio.h>
#include "jkimg.cuh"

__global__ void computeImageGradient(
    float* Ix, float* Iy, float* I, int nx, int ny)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;

    Ix[idx] = computeGradientX(I, 0, x, y, nx, ny);
    Iy[idx] = computeGradientY(I, 0, x, y, nx, ny);
}

__global__ void computeFlow(
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    float* Iw, float* Ix, float* Iy, float* rho_fixed,
    float* p1, float* p2, float* q1, float* q2,
    float lambd, float theta,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    // ROF (update v in the paper, GX,GY in this code)
    int idx = x + y*nx;

    float rho, grad, It, g1, g2;

    rho = rho_fixed[idx] + Ix[idx]*FX[idx] + Iy[idx]*FY[idx];
    grad = Ix[idx]*Ix[idx] + Iy[idx]*Iy[idx];

    if (rho <= -lambd*theta*grad) {
        g1 = lambd*theta * Ix[idx];
        g2 = lambd*theta * Iy[idx];
    } else if (rho > lambd*theta*grad) {
        g1 = -lambd*theta * Ix[idx];
        g2 = -lambd*theta * Iy[idx];
    } else {
        if (grad < 1e-10) {
            //g1 = g2 = 0;
            grad = grad + 1e-10;
            g1 = -rho/grad * Ix[idx];
            g2 = -rho/grad * Iy[idx];
        } else {
            g1 = -rho/grad * Ix[idx];
            g2 = -rho/grad * Iy[idx];
        }
    }

    GX[idx] = FX[idx] + g1;
    GY[idx] = FY[idx] + g2;

    // divergence of dual variables
    float div_p, div_q;
    div_p = computeDivergence(p1, p2, x, y, nx, ny);
    div_q = computeDivergence(q1, q2, x, y, nx, ny);

    // compute flows
    FX[idx] = GX[idx] + theta * div_p;
    FY[idx] = GY[idx] + theta * div_q;

    __syncthreads();

    // gradient of primal variables
    float fxx, fxy, fyx, fyy;
    fxx = computeGradientX(FX, 1, x, y, nx, ny);
    fxy = computeGradientY(FX, 1, x, y, nx, ny);
    fyx = computeGradientX(FY, 1, x, y, nx, ny);
    fyy = computeGradientY(FY, 1, x, y, nx, ny);
    //computeGradient(fxx, fxy, FX, 1, x, y, nx, ny);
    //computeGradient(fyx, fyy, FY, 1, x, y, nx, ny);

    // update dual variables
    const float tau = 0.25;
    const float tau_over_theta = tau / theta;
    const float denom1 = 1. + tau_over_theta * hypot(fxx, fxy);
    const float denom2 = 1. + tau_over_theta * hypot(fyx, fyy);

//    if (x == 5 and y == 1)
//        printf("rho:%f, grad:%f, It:%f, %f, %f\\n", rho, grad, It, div_p, denom1);


    p1[idx] = (p1[idx] + tau_over_theta * fxx) / denom1;
    p2[idx] = (p2[idx] + tau_over_theta * fxy) / denom1;
    q1[idx] = (q1[idx] + tau_over_theta * fyx) / denom2;
    q2[idx] = (q2[idx] + tau_over_theta * fyy) / denom2;
}

__global__ void updatePrimal(
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    float* Iw, float* Ix, float* Iy, float* rho_fixed,
    float* p1, float* p2, float* q1, float* q2,
    float lambd, float theta,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    // ROF (update v in the paper, GX,GY in this code)
    int idx = x + y*nx;

    float rho, grad, It, g1, g2;

    rho = rho_fixed[idx] + Ix[idx]*FX[idx] + Iy[idx]*FY[idx];
    grad = Ix[idx]*Ix[idx] + Iy[idx]*Iy[idx];

    if (rho <= -lambd*theta*grad) {
        g1 = lambd*theta * Ix[idx];
        g2 = lambd*theta * Iy[idx];
    } else if (rho > lambd*theta*grad) {
        g1 = -lambd*theta * Ix[idx];
        g2 = -lambd*theta * Iy[idx];
    } else {
        if (grad < 1e-7) {
            //g1 = g2 = 0;
            grad = grad + 1e-7;
            g1 = -rho/grad * Ix[idx];
            g2 = -rho/grad * Iy[idx];
        } else {
            g1 = -rho/grad * Ix[idx];
            g2 = -rho/grad * Iy[idx];
        }
    }

    GX[idx] = FX[idx] + g1;
    GY[idx] = FY[idx] + g2;

    // divergence of dual variables
    float div_p, div_q;
    div_p = computeDivergence(p1, p2, x, y, nx, ny);
    div_q = computeDivergence(q1, q2, x, y, nx, ny);

    // compute flows
    FX[idx] = GX[idx] + theta * div_p;
    FY[idx] = GY[idx] + theta * div_q;
}

__global__ void updateDual(
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    float* Iw, float* Ix, float* Iy, float* rho_fixed,
    float* p1, float* p2, float* q1, float* q2,
    float lambd, float theta,
    int nx, int ny, int nc
)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    // ROF
    int idx = x + y*nx;

    // gradient of primal variables
    float fxx, fxy, fyx, fyy;
    fxx = computeGradientX(FX, 1, x, y, nx, ny);
    fxy = computeGradientY(FX, 1, x, y, nx, ny);
    fyx = computeGradientX(FY, 1, x, y, nx, ny);
    fyy = computeGradientY(FY, 1, x, y, nx, ny);

    // update dual variables
    float tau = 0.25;
    float tau_over_theta = tau / theta;
    float denom1 = 1. + tau_over_theta * hypot(fxx, fxy);
    float denom2 = 1. + tau_over_theta * hypot(fyx, fyy);

//    if (x == 5 and y == 1)
//        printf("rho:%f, grad:%f, It:%f, %f, %f\\n", rho, grad, It, div_p, denom1);

    p1[idx] = (p1[idx] + tau_over_theta * fxx) / denom1;
    p2[idx] = (p2[idx] + tau_over_theta * fxy) / denom1;
    q1[idx] = (q1[idx] + tau_over_theta * fyx) / denom2;
    q2[idx] = (q2[idx] + tau_over_theta * fyy) / denom2;
}


/******************************
** compute warp images of I2[x + FX[x]]
*******************************/

__global__ void warp(
    float* I2w, float* Iw, float* Ix, float* Iy, float* rho_fixed,
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    int nx, int ny, int nc)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;

    // printf("x:%d, y:%d, idx:%d ", x, y, idx);

    // initialization
    GX[idx] = FX[idx];
    GY[idx] = FY[idx];

    // compute warping (I2w, I2x, I2y)
    Iw[idx] = 0;
    float I1_ = 0;

    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c * nx * ny;
        I2w[idxc] = interp2_bicubic_at(I2+c*nx*ny, x+FX[idx], y+FY[idx], nx, ny, 0.);
        Iw[idx] += I2w[idxc];
        I1_ += I1[idxc];
    }

    // normalization w.r.t. channels
    Iw[idx] /= (float)nc;

    __syncthreads();

    // compute derivatives of image w.r.t. x and y directions

    float Iw_=0, Ix_=0, Iy_=0, dx ,dy;

    for (int c=0; c < nc; c++)
    {
        computeGradient(dx, dy, I2w+c*nx*ny, 0, x, y, nx, ny);
        Ix_ += dx;
        Iy_ += dy;
    }

    // normalize w.r.t. channels
    Ix[idx] = Ix_ / (float)nc;
    Iy[idx] = Iy_ / (float)nc;
    I1_ = I1_ / (float)nc;

    rho_fixed[idx] = Iw[idx] - I1_ - Ix[idx]*FX[idx] - Iy[idx]*FY[idx];
}

__global__ void warp2(
    float* Iw, float* Ix, float* Iy, float* rho_fixed,
    float* I2w, float* I2x, float* I2y,
    float* I1, float* I2, float* FX, float* FY, float* GX, float* GY,
    int nx, int ny, int nc)
{
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if (x < 0 || x >= nx || y < 0 || y >= ny) {
        return;
    }

    int idx = x + y * nx;

    // initialization
    GX[idx] = FX[idx];
    GY[idx] = FY[idx];

    // compute warping (I2w, I2x, I2y)
    Iw[idx] = 0;
    Ix[idx] = 0;
    Iy[idx] = 0;

    float I1_ = 0;

    for (int c=0; c < nc; c++)
    {
        int idxc = idx + c * nx * ny;

        Iw[idx] += interp2_bicubic_at(I2+c*nx*ny, x+FX[idx], y+FY[idx], nx, ny, 0.);
        Ix[idx] += interp2_bicubic_at(I2x+c*nx*ny, x+FX[idx], y+FY[idx], nx, ny, 0.);
        Iy[idx] += interp2_bicubic_at(I2y+c*nx*ny, x+FX[idx], y+FY[idx], nx, ny, 0.);

        // for computing rho
        I1_ += I1[idxc];
    }

    // normalization w.r.t. channels
    Iw[idx] /= (float)nc;
    Ix[idx] /= (float)nc;
    Iy[idx] /= (float)nc;

    // compute fixed value of rho
    I1_ = I1_ / (float)nc;
    rho_fixed[idx] = Iw[idx] - I1_ - Ix[idx]*FX[idx] - Iy[idx]*FY[idx];
}