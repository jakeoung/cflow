##############################
# Import necessary modules
##############################
import cffi
from utils import flowlib
import numpy as np
import os
import time
import math

from PIL import Image
from scipy import misc
from scipy import ndimage
from scipy import interpolate

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.add_argument('--model', type=str, default='../', help='brox')
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default='50', help='iteration number')
parser.add_argument('-n2', '--niterinner', type=int, default='100', help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default='3', help='number of scales in pyramid')

parser.add_argument('--lambd', type=float, default='0.15', help='data fidelity weight')  # rubberwhale 0.3
parser.add_argument('--theta', type=float, default='0.3', help='gradient')

args, unparsed = parser.parse_known_args()

if args.lambd == 0.15 and args.seq == 4:
    args.lambd = 0.3

fResult = time.strftime('%m%d_')
dic = vars(args)
for key in dic:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')
parser.add_argument('--root', type=str, default='../', help='project folder')
parser.add_argument('--gpu', type=int, default=0, help='gpu device number')
parser.add_argument('--dResult', type=str, default='../result/base/')
parser.add_argument('-v', '--verbose', action='store_true', default=0, help='verbose mode')

args = parser.parse_args(namespace=args)

fResult_ = os.path.join(args.dResult, fResult)

print(args)
print(fResult_)

##############################
# Import external c module
# There are two ways
##############################


##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon', 'Grove2', 'Grove3', 'Hydrangea',
            'RubberWhale', 'Urban2', 'Urban3', 'Venus']

dir_data = 'data/other-data-two/'

im1ori = misc.imread(os.path.join(args.root, dir_data + seq_list[args.seq] + '/frame10.png'))

im2ori = misc.imread(os.path.join(args.root, dir_data + seq_list[args.seq] + '/frame11.png'))

# pre-smothing
im1ori = ndimage.gaussian_filter(im1ori, sigma=0.8)
im2ori = ndimage.gaussian_filter(im2ori, sigma=0.8)

# im1ori = Image.open(os.path.join(args.root,
#     'data/other-data-gray-two/'+seq_list[args.seq]+'/frame10.png'))

# im2ori = Image.open(os.path.join(args.root,
#     'data/other-data-gray-two/'+seq_list[args.seq]+'/frame11.png'))

# im1ori = np.array(im1ori)
# im2ori = np.array(im2ori)

gt = flowlib.read_flow(os.path.join(args.root,
                                    'data/other-gt-flow/' + seq_list[args.seq] + '/flow10.flo'))

# gt.astype(dtype=np.float32, copy=False)
gt1 = gt[:, :, 0].astype(dtype=np.float32)
gt2 = gt[:, :, 1].astype(dtype=np.float32)

nIterInner = 10
nIterGS = 10

dMinLambda = 0.91
dMaxLambda = 0.999

bGT = 0

nRow, nCol, nCha = np.atleast_3d(im1ori).shape

FX = np.zeros([nRow, nCol], dtype=np.float32)
FY = np.zeros([nRow, nCol], dtype=np.float32)

##############################
# compute flow, using pyraimd
##############################
I1s = im1ori
I2s = im2ori

im1 = np.atleast_3d(I1s)
im2 = np.atleast_3d(I2s)

im1 = im1.transpose(2, 0, 1)
im2 = im2.transpose(2, 0, 1)

im1 = im1.astype(np.float32)
im2 = im2.astype(np.float32)

max_ = max(im1.max(), im2.max())
min_ = min(im1.min(), im2.min())

im1 = 255.0 * (im1 - min_) / (max_ - min_)
im2 = 255.0 * (im2 - min_) / (max_ - min_)

im1 = np.ascontiguousarray(im1, dtype=np.float32)
im2 = np.ascontiguousarray(im2, dtype=np.float32)

# im1 = im1.reshape([-1])
# im2 = im2.reshape([-1])

#############################
# pycuda
##############################
import pycuda.driver as cuda
import pycuda.gpuarray as pc
import pycuda.autoinit
from pycuda.compiler import SourceModule

im1_gpu = cuda.mem_alloc(im1.size * im1.dtype.itemsize)
im2_gpu = cuda.mem_alloc(im2.size * im2.dtype.itemsize)
FX_gpu = cuda.mem_alloc(FX.size * FX.dtype.itemsize)
FY_gpu = cuda.mem_alloc(FY.size * FY.dtype.itemsize)

# FX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
# FY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

GX_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
GY_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

I2w_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
I2x_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
I2y_gpu = pc.zeros([nCha, nRow, nCol], dtype=np.float32)
Iw_gpu = pc.zeros([nRow, nCol], dtype=np.float32)  # normalized warping values
Ix_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
Iy_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
rho_fixed_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

p1_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
p2_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
q1_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
q2_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
ux_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
uy_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
vx_gpu = pc.zeros([nRow, nCol], dtype=np.float32)
vy_gpu = pc.zeros([nRow, nCol], dtype=np.float32)

cuda.memcpy_htod(im1_gpu, im1)
cuda.memcpy_htod(im2_gpu, im2)
cuda.memcpy_htod(FX_gpu, FX)
cuda.memcpy_htod(FY_gpu, FY)

cuda_code = ''
with open(os.path.join(args.root, 'tvl1/src/flow_hh.cu'), 'r') as f:
    cuda_code += f.read()

mod = SourceModule(cuda_code,
                   include_dirs=['/home01/users/jakeoung/work/cflow/tvl1/src'])

nRow = np.int32(nRow)
nCol = np.int32(nCol)
nCha = np.int32(nCha)

blockSize = (32, 32, 1)
gridSize = ((int)((nRow + 1) / blockSize[0]), (int)((nCol + 1) / blockSize[1]))
print('blockSize:', blockSize, ', gridSize:', gridSize)

computeImageGradient = mod.get_function("computeImageGradient", )
computeFlow = mod.get_function("computeFlow", )
updatePrimal = mod.get_function("updatePrimal", )
updateDual = mod.get_function("updateDual", )
warp = mod.get_function("warp")
warp2 = mod.get_function("warp2")

pure_pixelwise = 0;

computeImageGradient(I2x_gpu, I2y_gpu, im2_gpu, nCol, nRow, block=blockSize, grid=gridSize);

for it in range(args.niter):
    # if pure_pixelwise:
    #     warp(I2w_gpu, Iw_gpu, Ix_gpu, Iy_gpu, rho_fixed_gpu,
    #          im1_gpu, im2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
    #          nCol, nRow, nCha,
    #          block=blockSize, grid=gridSize)
    # else:
    warp2(Iw_gpu, Ix_gpu, Iy_gpu, rho_fixed_gpu,
          I2w_gpu, I2x_gpu, I2y_gpu,
          im1_gpu, im2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
          nCol, nRow, nCha,
          block=blockSize, grid=gridSize)

    for j in range(args.niterinner):
        if pure_pixelwise:
            computeFlow(im1_gpu, im2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
                        Iw_gpu, Ix_gpu, Iy_gpu, rho_fixed_gpu,
                        p1_gpu, p2_gpu, q1_gpu, q2_gpu,
                        np.float32(args.lambd), np.float32(args.theta),
                        nCol, nRow, nCha,
                        block=blockSize, grid=gridSize
                        )
        else:
            updatePrimal(im1_gpu, im2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
                         Iw_gpu, Ix_gpu, Iy_gpu, rho_fixed_gpu,
                         p1_gpu, p2_gpu, q1_gpu, q2_gpu,
                         np.float32(args.lambd), np.float32(args.theta),
                         nCol, nRow, nCha,
                         block=blockSize, grid=gridSize)
            updateDual(im1_gpu, im2_gpu, FX_gpu, FY_gpu, GX_gpu, GY_gpu,
                       Iw_gpu, Ix_gpu, Iy_gpu, rho_fixed_gpu,
                       p1_gpu, p2_gpu, q1_gpu, q2_gpu,
                       np.float32(args.lambd), np.float32(args.theta),
                       nCol, nRow, nCha,
                       block=blockSize, grid=gridSize)

    if (it % 2 == 0):
        cuda.memcpy_dtoh(FX, FX_gpu)
        cuda.memcpy_dtoh(FY, FY_gpu)
        ape, aae = flowlib.flow_error(gt[:, :, 0], gt[:, :, 1], FX, FY)
        print('iter: %d, APE: %f, AAE: %f, max_FX: %f' % (it, ape, aae, FX.max()))

cuda.memcpy_dtoh(FX, FX_gpu)
cuda.memcpy_dtoh(FY, FY_gpu)

# compute_gradient
# print(FX)

##############################
# write output
##############################
ape, aae = flowlib.flow_error(gt[:, :, 0], gt[:, :, 1], FX, FY)
print('APE: %f, AAE: %f, max_FX: %f' % (ape, aae, FX.max()))

flow_img = flowlib.flow_to_image(FX, FY)

if args.verbose:
    im = Image.fromarray(flow_img).show()

if os.path.exists(args.dResult):
    np.save(fResult_, [FX, FY, ape, aae])
