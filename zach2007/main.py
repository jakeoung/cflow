##############################
# Import necessary modules
##############################
import cffi
import numpy as np
import os
import time
import math

from PIL import Image
from scipy import misc
from scipy import ndimage
from scipy import interpolate

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.add_argument('--model', type=str, default='tvl1', help='tvl1')
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default='20', help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default='1', help='number of scales in pyramid')

parser.add_argument('--theta', type=float, default='0.3', help='smooth')
parser.add_argument('--lambd', type=float, default='0.3', help='gradient')

args, unparsed = parser.parse_known_args()
fResult = time.strftime('%m%d_')
dic = vars(args)
for key in dic:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')
parser.add_argument('--root', type=str, default='../', help='project folder')
parser.add_argument('--dResult', type=str, default='../result/base/')
parser.add_argument('-v','--verbose', action='store_true', default=1, help='verbose mode')

args = parser.parse_args(namespace=args)

fResult_ = os.path.join(args.dResult, fResult)

print(args)
print(fResult_)

os.sys.path.append(os.path.join(args.root,'code/utils'))
import flowlib

##############################
# Import external c module
# There are two ways
##############################
ffi = cffi.FFI()
ffi.cdef("""void Dual_TVL1_optic_flow_multiscale(
		float *I0,           // source image
		float *I1,           // target image
		float *u1,           // x component of the optical flow
		float *u2,           // y component of the optical flow
		const int   nxx,     // image width
		const int   nyy,     // image height
		const float tau,     // time step
		const float lambda,  // weight parameter for the data term
		const float theta,   // weight parameter for (u - v)²
		const int   nscales, // number of scales
		const float zfactor, // factor for building the image piramid
		const int   warps,   // number of warpings per scale
		const float epsilon, // tolerance for numerical convergence
		const bool  verbose  // enable/disable the verbose mode
);""")

fLib = os.path.join(args.root, "zach2007/src/tvl1flow.so")
lib = ffi.dlopen(fLib)

##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon','Grove2','Grove3','Hydrangea',
            'RubberWhale','Urban2','Urban3','Venus']

im1ori = Image.open(os.path.join(args.root,
    'data/other-data-gray-two/'+seq_list[args.seq]+'/frame10.png'))

im2ori = Image.open(os.path.join(args.root,
    'data/other-data-gray-two/'+seq_list[args.seq]+'/frame11.png'))

im1ori = np.array(im1ori)
im2ori = np.array(im2ori)

gt = flowlib.read_flow(os.path.join(args.root,
    'data/other-gt-flow/'+seq_list[args.seq]+'/flow10.flo'))

# gt.astype(dtype=np.float32, copy=False)
gt1 = gt[:,:,0].astype(dtype=np.float32)
gt2 = gt[:,:,1].astype(dtype=np.float32)


nIterInner=10
nIterGS=10

dMinLambda=0.91
dMaxLambda=0.999

bGT=0

nRow, nCol, nCha = np.atleast_3d(im1ori).shape

FX = np.zeros([nRow, nCol], dtype=np.float32)
FY = np.zeros([nRow, nCol], dtype=np.float32)

##############################
# compute flow, using pyraimd
##############################
I1s = im1ori
I2s = im2ori

im1 = np.atleast_3d(I1s)
im2 = np.atleast_3d(I2s)

im1 = im1.transpose(2, 0, 1)
im2 = im2.transpose(2, 0, 1)

im1 = im1.astype(np.float32)
im2 = im2.astype(np.float32)

im1 = (im1 - im1.min()) / (im1.max() - im1.min())
im2 = (im2 - im2.min()) / (im2.max() - im2.min())

im1 = im1.reshape([-1])
im2 = im2.reshape([-1])

p_img1 = ffi.cast("float *", im1.ctypes.data)
p_img2 = ffi.cast("float *", im2.ctypes.data)
p_FX   = ffi.cast("float *", FX.ctypes.data)
p_FY   = ffi.cast("float *", FY.ctypes.data)

lib.Dual_TVL1_optic_flow_multiscale(p_img1, p_img2, p_FX, p_FY, nCol, nRow,
                        0.25, args.lambd, args.theta, args.pyramid, 0.75,
                        args.niter, 0.005, args.verbose)

##############################
# write output
##############################
ape, aae = flowlib.flow_error(gt[:,:,0], gt[:,:,1], FX, FY)
print('APE: %f, AAE: %f, max_FX: %f' % (ape, aae, FX.max()))

flow_img = flowlib.flow_to_image(FX, FY)

if args.verbose:
    im = Image.fromarray(flow_img).show()

if os.path.exists(args.dResult):
    np.save(fResult_, [FX, FY, ape, aae] )