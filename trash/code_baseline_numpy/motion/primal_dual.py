__author__      = "Ja-Keoung Koo"

import math

from scipy import misc
from scipy import ndimage

from .util import *
from .flowlib import *


class Motion(object):
    """
    model:
    \min_u \int_\Omega (\nabla I . w + I_t)^l + |\nabla w|_{epsHuberReg} dx

    l=1: L1 data term
    l=2: L2 data term
    |\nabla u|_{epsHuberReg} : Huber total variation
    """

    def __init__(self, I1ori, I2ori, model, niter, lambd, tau, gt):
        self.I1ori = I1ori
        self.I2ori = I2ori
        self.niter = niter
        self.lambd = lambd
        self.tau = tau

        self.w = 0
        self.gt = gt

        self.FX = 0
        self.FY = 0

        self.nCha = I1ori.shape[0]
        self.nRowOri = I1ori.shape[1]
        self.nColOri = I1ori.shape[2]

        # for TV regularization, epsHuberReg should be 0
        self.epsHuberReg = 0.

        # set model
        if model == 'HS':
            self.model_data = 'L2'
            self.model_reg  = 'L2'
        elif model == 'TVL1':
            self.model_data = 'L1'
            self.model_reg  = 'TV'
        elif model =='HL1':
            self.model_data = 'L1'
            self.model_reg = 'HTV'
            self.epsHuberReg = 1.0

        self.plot_trigger = 10


    def do_pyramid(self, pyramid=1):

        nRows = math.ceil(self.nRowOri / math.pow(2, (pyramid - 1)))
        nCols = math.ceil(self.nColOri / math.pow(2, (pyramid - 1)))

        self.FX = np.zeros([nRows, nCols], dtype=np.float32)
        self.FY = np.zeros([nRows, nCols], dtype=np.float32)

        bGT = 0

        ##############################
        # compute flow, using pyraimd
        ##############################
        for s in range(pyramid):
            I1s = misc.imresize(self.I1ori, 1 / math.pow(2, pyramid - s - 1), 'cubic')
            I2s = misc.imresize(self.I2ori, 1 / math.pow(2, pyramid - s - 1), 'cubic')

            im1 = np.atleast_3d(I1s).transpose(2, 0, 1)
            im2 = np.atleast_3d(I2s).transpose(2, 0, 1)

            im1 = im1.astype(np.float32)
            im2 = im2.astype(np.float32)

            im1 = (im1 - im1.min()) / (im1.max() - im1.min())
            im2 = (im2 - im2.min()) / (im2.max() - im2.min())

            if s + 1 == pyramid and isinstance(self.gt, int) == False:
                bGT = 1

            print('optimize for scale %f:' % (1 / math.pow(2, pyramid - s -1)))
            self.primal_dual(im1, im2, tau=self.tau, lambd=self.lambd, bGT=bGT)

            if s + 1 == pyramid:
                break

            # nRows = math.ceil(nRow / math.pow(2, (args.pyramid - s - 2)))
            # nCols = math.ceil(nCol / math.pow(2, (args.pyramid - s - 2)))

            ratio = 2
            self.FX = ratio * ndimage.zoom(self.FX, 2, order=0)  # nearest
            self.FY = ratio * ndimage.zoom(self.FY, 2, order=0)

            self.FX.astype(dtype=np.float32, copy=False)
            self.FY.astype(dtype=np.float32, copy=False)

    def primal_dual(self, I1, I2, tau, lambd, bGT=False):
        """
        optimizer per one scale
        """

        ##############################
        # initialization
        ##############################
        nCha, nRow, nCol = I1.shape

        FX0 = np.copy(self.FX)
        FY0 = np.copy(self.FY)

        p1 = np.zeros([nRow, nCol])
        p2 = np.zeros([nRow, nCol])
        q1 = np.zeros([nRow, nCol])
        q2 = np.zeros([nRow, nCol])

        ux = np.zeros([nRow, nCol])
        uy = np.zeros([nRow, nCol])
        vx = np.zeros([nRow, nCol])
        vy = np.zeros([nRow, nCol])

        # div_p  = np.zeros([nRow, nCol])
        # div_q  = np.zeros([nRow, nCol])

        I2_warp = np.zeros(I1.shape)
        I2x     = np.zeros(I1.shape)
        I2y     = np.zeros(I1.shape)

        sigma   = 1 / (8 * tau)
        energy  = np.zeros(self.niter, dtype=np.float32)

        weightTV = 1 - self.lambd
        nInnerIter = 10

        K = np.array([[-1, 9, -45, 0, 45, -9, 1]], float) / 60 # Bruhn, IJCV05
        # K = [-1 1]

        ##############################
        # pre-processing
        ##############################

        # pre-warping
        x       = np.linspace(1, I1.shape[2], I1.shape[2])
        y       = np.linspace(1, I1.shape[1], I1.shape[1])
        xx, yy  = np.meshgrid(x,y)

        # xx, yy  = np.meshgrid(x, y)
        for cc in range(self.nCha):
            I2_warp[cc,:,:] = interp2linear(I2[cc,:,:], xx+self.FX, yy+self.FY, extrapval=np.nan)

        I2_warp[np.isnan(I2_warp)] = I1[np.isnan(I2_warp)]

        ##############################
        # optimize start
        ##############################

        # warping iterations
        for i in range(self.niter):
            for cc in range(self.nCha):
                I2x[cc,:,:] = ndimage.correlate(I2_warp[cc,:,:], K, mode='reflect')
                I2y[cc,:,:] = ndimage.correlate(I2_warp[cc,:,:], np.transpose(K), mode='reflect')

            I2t = I2_warp - I1

            # mean due to color
            Ix = np.mean(I2x, 0)
            Iy = np.mean(I2y, 0)
            It = np.mean(I2t, 0)

            I2_amp = np.maximum(1e-09, Ix*Ix + Iy*Iy)

            for j in range(nInnerIter):
                ## step1: primal update proxG
                FX_prev = self.FX.copy()
                FY_prev = self.FY.copy()

                # compute divergence
                div_p = div(p1, p2)
                div_q = div(q1, q2)

                # update u,v
                self.FX = self.FX + tau * div_p
                self.FY = self.FY + tau * div_q

                # update
                if self.model_data == 'L2':
                    w     = w

                elif self.model_data == 'L1':
                    rho = It + (self.FX-FX0)*Ix + (self.FY-FY0)*Iy

                    idx1 = rho < -tau*lambd*I2_amp
                    idx2 = rho >  tau*lambd*I2_amp
                    idx3 = ~( idx1 + idx2)

                    self.FX[idx1] = self.FX[idx1] + tau*lambd*Ix[idx1]
                    self.FX[idx2] = self.FX[idx2] - tau*lambd*Ix[idx2]
                    self.FX[idx3] = self.FX[idx3] - rho[idx3]*Ix[idx3]/I2_amp[idx3]

                    self.FY[idx1] = self.FY[idx1] + tau*lambd*Iy[idx1]
                    self.FY[idx2] = self.FY[idx2] - tau*lambd*Iy[idx2]
                    self.FY[idx3] = self.FY[idx3] - rho[idx3]*Iy[idx3]/I2_amp[idx3]

                ## step2: extrapolation
                FX_bar = 2. * self.FX - FX_prev
                FY_bar = 2. * self.FY - FY_prev

                ## step3: dual update
                ux, uy = grad(FX_bar)
                vx, vy = grad(FY_bar)

                # forward step
                p1 = (p1 + sigma * ux) / (1. + sigma * self.epsHuberReg / weightTV)
                p2 = (p2 + sigma * uy) / (1. + sigma * self.epsHuberReg / weightTV)
                q1 = (q1 + sigma * vx) / (1. + sigma * self.epsHuberReg / weightTV)
                q2 = (q2 + sigma * vy) / (1. + sigma * self.epsHuberReg / weightTV)

                # backproject to dual isonorm unitball, anisotropic norm
                norms = np.absolute(p1)
                p1 = p1 / np.maximum(1., norms / weightTV)
                norms = np.absolute(p2)
                p2 = p2 / np.maximum(1., norms / weightTV)

                norms = np.absolute(q1)
                q1 = q1 / np.maximum(1., norms / weightTV)
                norms = np.absolute(q2)
                q2 = q2 / np.maximum(1., norms / weightTV)

            ## warping
            for cc in range(self.nCha):
                I2_warp[cc,...] = interp2linear(I2[cc,...], xx+self.FX, yy+self.FY, extrapval=np.nan)

            I2_warp[np.isnan(I2_warp)] = I1[np.isnan(I2_warp)]

            FX0 = self.FX.copy()
            FY0 = self.FY.copy()

            ## stopping criterion
            energy[i]  = lambd*np.sum(abs(rho[:]))
            energy[i] += weightTV*np.sum(np.sum(np.sqrt(ux*ux + uy*uy) + np.sqrt( vx*vx + vy*vy )))
            energy[i] /= I1.size

            ## plot
            if i % self.plot_trigger == 0:
                if bGT:
                    ape, aae = flow_error(self.gt[:, :, 0], self.gt[:, :, 1], self.FX, self.FY)
                    print("(%4d / %4d), energy: %f, FX_max: %f, ape: %f" %
                          (i, self.niter, energy[i], self.FX.max(), ape))
                else:
                    print("(%4d / %4d), energy: %f, FX_max: %f" %
                          (i, self.niter, energy[i], self.FX.max()))

        print("done")
        return energy