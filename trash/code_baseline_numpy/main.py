from motion import *
import numpy as np
import os
import time
import math

from PIL import Image
from scipy import misc
from scipy import ndimage

import torch

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.add_argument('-m','--model', type=str, default='TVL1', help='motion: HS, TVL1, HL1')
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default='20', help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default='2', help='number of scales in pyramid')

parser.add_argument('-t', '--tau', type=float, default='0.25', help='step size in Chambolle-Pock')

parser.add_argument('--lambd', type=float, default='0.9', help='constant lambda')
parser.add_argument('--dHuberRegular', type=float, default='1.0')

args, unparsed = parser.parse_known_args()
fResult = time.strftime('%m%d_')
dic = vars(args)
for key in dic:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')
parser.add_argument('--root', type=str, default='../', help='work folder')
parser.add_argument('--dResult', type=str, default='../result/pd/')
parser.add_argument('-v','--verbose', type=int, default=0, metavar='N', help='verbose mode')

args = parser.parse_args(namespace=args)

print(args)

##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon','Grove2','Grove3','Hydrangea',
            'RubberWhale','Urban2','Urban3','Venus']

im1ori = misc.imread(os.path.join(args.root,
    'data/other-data-two/'+seq_list[args.seq]+'/frame10.png'))

im2ori = misc.imread(os.path.join(args.root,
    'data/other-data-two/'+seq_list[args.seq]+'/frame11.png'))

gt = flowlib.read_flow(os.path.join(args.root,
    'data/other-gt-flow/'+seq_list[args.seq]+'/flow10.flo'))

if isinstance(gt, int) == False:
    gt1 = gt[:,:,0].astype(dtype=np.float32)
    gt2 = gt[:,:,1].astype(dtype=np.float32)

im1ori = np.atleast_3d(im1ori).transpose(2,0,1)
im2ori = np.atleast_3d(im2ori).transpose(2,0,1)

##############################
# model selection
##############################
motion = Motion(im1ori, im2ori, model=args.model, niter=args.niter,
                    lambd=args.lambd, tau=args.tau, gt=gt)

motion.do_pyramid(args.pyramid)
FX = motion.FX
FY = motion.FY

##############################
# write output
##############################
if isinstance(gt, int) == False:
    ape, aae = flowlib.flow_error(gt[:,:,0], gt[:,:,1], FX, FY)
    print('ape: %f, aae: %f, max_FX: %f' % (ape, aae, FX.max()))

if args.verbose:
    flow_img = flowlib.flow_to_image(FX, FY)
    im = Image.fromarray(flow_img).show()

if os.path.exists(args.dResult):
    np.save( os.path.join(args.dResult, fResult), [FX, FY, ape, aae] )