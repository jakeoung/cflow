# Python wrappers of C-based optical flow

This repository aims to practice the usages of CFFI.

## HuberHuber (cpu)

Pure C wrapper with compilation based on distutils module

- Compile C codes using `ffi.cdef, ffi.set_source, ffi.compile`
(This will generate a module)
- Import the module (e.g., `from _ext.lib import ffi, lib`)

```
# from root
$ cd code
$ make
$ python3 main.py
```

## HuberHuber (gpu)

Install `pycuda` package and run.

```
$ cd code_cu
$ python3 main.py
```

## brox2004

C++ wrapper with shared library

- Declare a extern C function in cpp file (e.g. `brox_optic_flow.cpp`)
- Generate a shared library file
- Import using `ffi.dlopen(.so)` in python file

```
# from root
$ cd brox2004/src/
$ make
$ cd ..
$ python3 main.py
```

## zach2007

```
# from root
$ cd zach2007/src/
$ make
$ cd ..
$ python3 main.py
```

## hs

```
# from root
$ cd hs/src/
$ make
$ cd ..
$ python3 main.py
```
