from cffi import FFI

import os
import argparse

parser = argparse.ArgumentParser("setup")
parser.add_argument("-n", "--no-openmp", action="store_true", help="disable using openMP")
args = parser.parse_args()
print(args)

extra_compile_args = ['-fPIC', '-g', '-Wall', '-O3']
extra_link_args = []

if args.no_openmp == False:
    extra_compile_args += ['-fopenmp']
    extra_link_args += ['-fopenmp']

ffi = FFI()

sources = ['src/' + f for f in os.listdir('src') if f.endswith('c')]
headers = ['src/' + f for f in os.listdir('src') if f.endswith('h')]

# sources = ["src/JKImg.c","src/motion_huberhuber.c"]
# headers = ["src/JKImg.h","src/motion_huberhuber.h"]

relative_to = './'
all_header_source = ''

for header in headers:
    with open(os.path.join(relative_to, header), 'r') as f:
        all_header_source += f.read() + '\n\n'

ffi.cdef(all_header_source)
ffi.set_source("_ext.lib", all_header_source,
    sources=sources,
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
    # extra_compile_args=['-Wall', '-O3', '-fopenmp']
    )

ffi.compile()
