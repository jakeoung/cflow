There are two ways to wrap my c++ files. To practice both ways, I leave them for my information.
In my Mac OS, OMP option doesn't working when using distutils. It is encouraged to learn both ways.

## Using distutils

We will generate external python module and import it.

```
$ make    # generate _ext folder
$ python3 main.py
```

## Make shared library on my own

We will make a new shared library and use it, directly. Use `(ffi.dlopen(''))`

```
$ cd src
$ make   # generate libtarget.so
$ cd ..
$ python3 main.py
```