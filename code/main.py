##############################
# Import external c module
# There are two ways
##############################
## 1. Using distutils
from _ext.lib import ffi, lib

## 2. Make shared library on my own
# import cffi
# ffi = cffi.FFI()
# ffi.cdef("void motion_huberhuber(const float* I1, const float* I2, float* , float*,\
# int ny, int nx, int nc, const int nIter, const int nIterInner,\
# int nIterGS, int adaptive, float dLambda, float dBetaLambda, float dMinLambda, float dMaxLambda,\
# float param1, float dHuberRegular, float dHuberData, float dWeightSplit, float dHuberLight,\
# float dWeightLight, int, const float*, const float*, int bVerbose);");
# lib = ffi.dlopen("src/libtarget.so")


##############################
# Import necessary modules
##############################
from utils import flowlib
import numpy as np
import os
import time
import math

from PIL import Image
from scipy import misc
from scipy import ndimage
from scipy import interpolate

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.set_defaults()
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default='20', help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default='2', help='number of scales in pyramid')

parser.add_argument('-a','--adaptive', type=int, default=0, help='adaptive method: 0,1,2')
parser.add_argument('--lambd', type=float, default='0.99', help='constant lambda')
parser.add_argument('--dBeta', type=float, default='20', help='beta in adaptive')
parser.add_argument('--param1', type=float, default='0', help='reserved param1')

parser.add_argument('--dHuberData', type=float, default='0.01')
parser.add_argument('--dHuberRegular', type=float, default='1.0')
parser.add_argument('-s', '--dWeightSplit', type=float, default='0.05', help='step size (small value is large step)')

parser.add_argument('--dHuberLight', type=float, default='0.05')
parser.add_argument('--dWeightLight', type=float, default='1')

args, unparsed = parser.parse_known_args()
fResult = time.strftime('%m%d_')
dic = vars(args)
for key in dic:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')
parser.add_argument('--root', type=str, default='../', help='work folder')
parser.add_argument('--dResult', type=str, default='../result/ours/')
parser.add_argument('-v','--verbose', action='store_true', default=0, help='verbose mode')

args = parser.parse_args(namespace=args)

fResult_ = os.path.join(args.dResult, fResult)

print(args)
print(fResult_)

##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon','Grove2','Grove3','Hydrangea',
            'RubberWhale','Urban2','Urban3','Venus']

im1ori = Image.open(os.path.join(args.root,
    'data/other-data-two/'+seq_list[args.seq]+'/frame10.png'))

im2ori = Image.open(os.path.join(args.root,
    'data/other-data-two/'+seq_list[args.seq]+'/frame11.png'))

im1ori = np.array(im1ori)
im2ori = np.array(im2ori)

# im1ori = misc.imread(os.path.join(args.root,
#     'data/other-data-two/'+seq_list[args.seq]+'/frame10.png'))
#
# im2ori = misc.imread(os.path.join(args.root,
#     'data/other-data-two/'+seq_list[args.seq]+'/frame11.png'))

gt = flowlib.read_flow(os.path.join(args.root,
    'data/other-gt-flow/'+seq_list[args.seq]+'/flow10.flo'))

# gt.astype(dtype=np.float32, copy=False)
gt1 = gt[:,:,0].astype(dtype=np.float32)
gt2 = gt[:,:,1].astype(dtype=np.float32)


nIterInner=10
nIterGS=10

dMinLambda=0.91
dMaxLambda=0.999

bGT=0

nRow, nCol, nCha = np.atleast_3d(im1ori).shape

nRows = math.ceil(nRow / math.pow(2, (args.pyramid-1)) )
nCols = math.ceil(nCol / math.pow(2, (args.pyramid-1)) )

FX = np.zeros([nRows, nCols], dtype=np.float32)
FY = np.zeros([nRows, nCols], dtype=np.float32)

# When using from _ext.lib import ffi, lib
p_GFX = ffi.cast("float *", gt1.ctypes.data)
p_GFY = ffi.cast("float *", gt2.ctypes.data)

##############################
# compute flow, using pyraimd
##############################
for s in range(args.pyramid):
    if s + 1 == args.pyramid:
        I1s = im1ori
        I2s = im2ori
    else:
        I1s = misc.imresize(im1ori, 1 / math.pow(2, args.pyramid-s-1), 'cubic')
        I2s = misc.imresize(im2ori, 1 / math.pow(2, args.pyramid-s-1), 'cubic')

    im1 = np.atleast_3d(I1s)
    im2 = np.atleast_3d(I2s)

    im1 = im1.transpose(2, 0, 1)
    im2 = im2.transpose(2, 0, 1)

    im1 = im1.astype(np.float32)
    im2 = im2.astype(np.float32)

    im1 = (im1 - im1.min()) / (im1.max() - im1.min())
    im2 = (im2 - im2.min()) / (im2.max() - im2.min())

    im1 = im1.reshape([-1])
    im2 = im2.reshape([-1])

    p_img1 = ffi.cast("float *", im1.ctypes.data)
    p_img2 = ffi.cast("float *", im2.ctypes.data)
    p_FX   = ffi.cast("float *", FX.ctypes.data)
    p_FY   = ffi.cast("float *", FY.ctypes.data)

    if s+1 == args.pyramid:
        bGT=1

    niter = (int)(args.niter / (float)(args.pyramid-s))
    print('optimize for scale: %f' % (1 / math.pow(2, (args.pyramid -s-1))))
    lib.motion_huberhuber(p_img1, p_img2, p_FX, p_FY, nRows, nCols, nCha, niter,
                         nIterInner, nIterGS, args.adaptive, args.lambd,
                         args.dBeta, dMinLambda, dMaxLambda, args.param1,
                         args.dHuberRegular, args.dHuberData, args.dWeightSplit, args.dHuberLight,
                         args.dWeightLight, bGT, p_GFX, p_GFY, args.verbose)


    if s+1 == args.pyramid:
        break

    nRows = math.ceil(nRow / math.pow(2, (args.pyramid - s-2)) )
    nCols = math.ceil(nCol / math.pow(2, (args.pyramid - s-2)) )
    ratio = 2

    FX = ratio * ndimage.zoom(FX, 2, order=0) # nearest
    FY = ratio * ndimage.zoom(FY, 2, order=0)
    # FX = ratio * misc.imresize(FX, [nRows, nCols], interp='nearest')
    # FY = ratio * misc.imresize(FY, [nRows, nCols], interp='nearest')

    FX.astype(dtype=np.float32, copy=False)
    FY.astype(dtype=np.float32, copy=False)

##############################
# write output
##############################
ape, aae = flowlib.flow_error(gt[:,:,0], gt[:,:,1], FX, FY)
print('APE: %f, AAE: %f, max_FX: %f' % (ape, aae, FX.max()))

flow_img = flowlib.flow_to_image(FX, FY)

if args.verbose:
    im = Image.fromarray(flow_img).show()

if os.path.exists(args.dResult):
    np.save(fResult_, [FX, FY, ape, aae] )