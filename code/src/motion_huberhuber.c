#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "JKImg.h"

#define MAX(a,b) (((a) >= (b)) ? (a) : (b))
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

float shrinkageScalar(float dValue, float dThreshold)
{
    float dValueAbs, dValueMax;
    dValueAbs = fabs(dValue);
    dValueMax = MAX(1.0, dValueAbs / dThreshold);
    return (1.0 - 1.0 / dValueMax) * dValue;
}

void updateR(float* R, float* pData, const float* FX, const float* FY,
    const float* FX0, const float* FY0, const float* I2t,
    const float* I2x, const float* I2y, const float* S, int ny, int nx, int nc,
    float dHuberData)
{
    #pragma omp parallel for collapse(3)
    for (int c=0; c < nc; c++) {
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {        
                float dIt, dIx, dIy, dR, dFX0, dFY0, dFX, dFY, dVal, dFidelity, dS;
                int idx  = x + y * nx;
                int idxc = x + y * nx + c * nx*ny;
                
                dFX = FX[idx];
                dFY = FY[idx];

                dFX0 = FX0[idx];
                dFY0 = FY0[idx];

                dIt = I2t[idxc];
                dIx = I2x[idxc];
                dIy = I2y[idxc];
                dS  =   S[idx];
                dVal = dIt + dIx * (dFX - dFX0) + dIy * (dFY - dFY0) - dS;

                dR = shrinkageScalar( dVal, dHuberData );

                dFidelity = fabs(dR) + pow(dVal-dR, 2.0) / (2.0*dHuberData);

                R[idxc] = dR;
                pData[idxc] = dFidelity;
            }
        }
    }
}

void updateLambda(float* pLambda, float* pData, float dBetaLambda, int method, float param1, float* pWeightTV, int ny, int nx, int nc)
{
    #pragma omp parallel for collapse(2)
    for (int y=0; y < ny; y++){
        for (int x=0; x < nx; x++)
        {    
            float dData = 0, dA, dLambda;
            
            for (int c=0; c < nc; c++) {
                int idxc = x + y * nx + c * nx*ny;
                dData += pData[idxc];
            }
            dData /= (float)nc;
            dA = exp( -dData / dBetaLambda );
            
             dLambda = dA - 0.01;
             pLambda[x+y*nx] = 1-dLambda;
            // pLambda[x+y*nx] = dA - param1;
            // pLambda[x+y*nx] = shrinkageScalar(dA, param1 );
            
            // pWeightTV[x+y*nx] = 1-pLambda[x+y*nx];
            pWeightTV[x+y*nx] = dLambda;
        }
    }
    printf("minL: %.3f, maxL: %.3f, ",
        minFromArray(pLambda, nx*ny), maxFromArray(pLambda, nx*ny));

}

/*
compute flows (FX, FY) for one scale of two images (I1, I2)
*/
void motion_huberhuber(const float* I1, const float* I2, float* FX, float* FY,
    int ny, int nx, int nc, const int nIter, const int nIterInner,
    int nIterGS, int adaptive, float dLambda, float dBetaLambda, float dMinLambda, float dMaxLambda,
    float param1, float dHuberRegular, float dHuberData, float dWeightSplit, float dHuberLight,
    float dWeightLight, int bGT, const float* GFX, const float*GFY, int bVerbose)

{
    int nSizeFlow = ny*nx;
    int nSizeImg = ny*nx*nc;

    int nMemory = 0;

    float* I2_warp  = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    float* I2x      = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    float* I2y      = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    float* I2t      = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    float* pData    = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    float* R        = (float*) calloc(nSizeImg,sizeof(float)); nMemory++;
    
    float* S        = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* FX0      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* FY0      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GX       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GY       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GXx      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GXy      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GYx      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* GYy      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PX       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PY       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* QX       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* QY       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* HX       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* HY       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* SX       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* SY       = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PHI      = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PHIX     = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PHIY     = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;

    float* PDIV     = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* QDIV     = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* PHIDIV   = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    
    // float* pLambda  = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* pLambda  = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* pWeightTV= (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* pLHS     = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* pRHS1    = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    float* pRHS2    = (float*) calloc(nSizeFlow,sizeof(float)); nMemory++;
    
    // initialization
    memcpy(FX0, FX, nSizeFlow*sizeof(float));
    memcpy(FY0, FY, nSizeFlow*sizeof(float));

    // impose F=G constraint
    memcpy(GX, FX, nSizeFlow*sizeof(float));
    memcpy(GY, FY, nSizeFlow*sizeof(float));

    float ape;

    if (bVerbose)
    {
        printf("ny:%d, nx:%d, nc:%d, nIter:%d, nIterInner:%d, nIterGS:%d\n", ny, nx, nc, nIter, nIterInner, nIterGS);
        printf("adaptive:%d, dLambda:%f, dBetaLambda:%f, dMinLambda:%f\n", adaptive, dLambda, dBetaLambda, dMinLambda);
        printf("param1:%f, dHuberRegular:%f, dHuberData:%f, dWeightSplit:%f\n", param1, dHuberRegular, dHuberData, dWeightSplit);
        printf("dHuberLight:%f, dWeightLight:%f\n", dHuberLight, dWeightLight);
    }

    /*** init lambda ***/
    if (adaptive == 0) {
        #pragma omp parallel for collapse(1)
        for (int i=0; i < nSizeFlow; i++)
        {
            pLambda[i] = (float)dLambda;
            pWeightTV[i] = 1 - (float)dLambda;
        }
    } else {
        updateR(R, pData, FX, FY, FX0, FY0, I2t, I2x, I2y, S, ny, nx, nc, dHuberData);
        updateLambda(pLambda, pData, dBetaLambda, adaptive, param1, pWeightTV, ny, nx, nc);
    }

    for (int it=0; it < nIter; it++)
    {
        printf("iter: %3d / %3d, ", it, nIter);;
        // decrease step size
        if (it % 50 == 0) {
            dWeightSplit *= 1.1;
        }
        memcpy(FX0, FX, nSizeFlow*sizeof(float));
        memcpy(FY0, FY, nSizeFlow*sizeof(float));

        for (int c=0; c < nc; c++) {
//            bicubic_interpolation_warp(I2 + c*nx*ny, FX, FY, I2_warp + c*nx*ny, nx, ny, 1);
            interp2_bicubic(I2 + c*nx*ny, FX, FY, I2_warp + c*nx*ny, ny, nx, I1 + c*nx*ny);
        }
        /* unit test of interp2_bicubic */
        // printf("%f, %f\t", I2_warp[400], I2_warp[800]);

        float err_data=0;
        #pragma omp parallel for collapse(1)
        for (int i=0; i < nSizeImg; i++) {
            I2t[i] = I2_warp[i] - I1[i];
            err_data += fabs(I2t[i]);
        }
        printf("err_data: %f, ", err_data / nSizeImg);
        
        for (int c=0; c < nc; c++) {
            computeGradient(I2_warp + c*nx*ny, 0, I2x + c*nx*ny, I2y + c*nx*ny, ny, nx);
        }
        
        /* unit test of computeGradient */
        // float *I2g  = I2_warp+1*nx*ny;
        // float *I2gx = I2x+1*nx*ny;
        // printf("%f %f %f\t", I2g[30],  I2g[31], I2gx[30]);

        for (int j=0; j < nIterInner; j++)
        {
            computeGradient(GX, 1, GXx, GXy, ny, nx);
            computeGradient(GY, 1, GYx, GYy, ny, nx);

            // printf("%f %f %f\n", GX[3], GX[4], GXx[3]);

            /*** update R ***/
            updateR(R, pData, FX, FY, FX0, FY0, I2t, I2x, I2y, S, ny, nx, nc, dHuberData);
            
            #pragma omp parallel for collapse(2)
            for (int y=0; y < ny; y++) {
                for (int x=0; x < nx; x++) {
                    /*** updatePQ ***/
                    float dPX, dPY, dQX, dQY, dRegularX, dRegularY;
                    int idx = x + y * nx;
                    dPX = shrinkageScalar(GXx[idx], dHuberRegular);
                    dPY = shrinkageScalar(GXy[idx], dHuberRegular);
                    dQX = shrinkageScalar(GYx[idx], dHuberRegular);
                    dQY = shrinkageScalar(GYy[idx], dHuberRegular);

                    // dRegularX = fabs(dPX) + fabs(dPY);
                    // dRegularX += ((dGXx - dPX)*(dGXx - dPX) + (dGXy - dPY)*(dGXy - dPY)) / (2.0 * dHuberRegular);

                    PX[idx] = dPX; PY[idx] = dPY; QX[idx] = dQX; QY[idx] = dQY;

                    /*** updateHXHY ***/
                    HX[idx] += FX[idx] - GX[idx];
                    HY[idx] += FY[idx] - GY[idx];

                    /*** updateFXFY ***/
                    float dPara = dHuberData * dWeightSplit;
                    float dIxIx, dIxIy, dIyIy, dItIx, dItIy, dIxR2=0., dIyR2=0.;
                    float dIxIx2=0., dIxIy2=0., dIyIy2=0., dItIx2=0., dItIy2=0.;
                    float dLambda, dNumerX, dNumerY, dDenom;
                    
                    float dGX_HX = GX[idx] - HX[idx];
                    float dGY_HY = GY[idx] - HY[idx];
                    float dS = S[idx];

                    dLambda = pLambda[idx];

                    for (int c=0; c < nc; c++)
                    {
                        float dR, dIt, dIx, dIy;
                        int idxc;
                        idxc = idx + c * nx*ny;

                        dR = R[idxc];
                        dIt = I2t[idxc];
                        dIx = I2x[idxc];
                        dIy = I2y[idxc];
                        
                        dIxIx   = dIx * dIx;
                        dIxIy   = dIx * dIy;
                        dIyIy   = dIy * dIy;
                        dItIx   = dIt * dIx;
                        dItIy   = dIt * dIy;
                        
                        dIxR2   += dIx * dR;
                        dIyR2   += dIy * dR;

                        // koo start
                        dIxR2   += dIx * dS;
                        dIyR2   += dIy * dS;
                        // koo end

                        dIxIx2  += dIxIx;
                        dIxIy2  += dIxIy;
                        dIyIy2  += dIyIy;
                        dItIx2  += dItIx;
                        dItIy2  += dItIy;
                    }
                    float dLX_RHS, dLY_RHS;

                    dIxR2   *= (dLambda / (float) (nc));
                    dIyR2   *= (dLambda / (float) (nc));
                    
                    dIxIx2  *= (dLambda / (float) (nc));
                    dIxIy2  *= (dLambda / (float) (nc));
                    dIyIy2  *= (dLambda / (float) (nc));
                    dItIx2  *= (dLambda / (float) (nc));
                    dItIy2  *= (dLambda / (float) (nc));

                    dLX_RHS = - dItIx2 + dIxR2 + dPara * dGX_HX + dIxIx2 * FX0[idx] + dIxIy2 * FY0[idx];
                    dLY_RHS = - dItIy2 + dIyR2 + dPara * dGY_HY + dIxIy2 * FX0[idx] + dIyIy2 * FY0[idx];
                    
                    dNumerX = (dPara + dIyIy2) * dLX_RHS - dIxIy2 * dLY_RHS;
                    dNumerY = (dPara + dIxIx2) * dLY_RHS - dIxIy2 * dLX_RHS;
                    
                    dDenom = (dPara + dIxIx2) * (dPara + dIyIy2) - dIxIy2 * dIxIy2;
                    
                    dNumerX /= dDenom;
                    dNumerY /= dDenom;

                    FX[idx] = dNumerX;
                    FY[idx] = dNumerY;
                }
            }

            /*** updateGXGY_GS_Term ***/
            computeDivergence(PX, PY, PDIV, ny, nx);
            computeDivergence(QX, QY, QDIV, ny, nx);
            
            #pragma omp parallel for collapse(1)
            for (int i=0; i < nSizeFlow; i++) {
                float dLHS, dRHS1, dRHS2;
                dLHS = (pWeightTV[i]) / (dHuberRegular * dWeightSplit);
                dRHS1 = FX[i] + HX[i] - dLHS * PDIV[i];
                dRHS2 = FY[i] + HY[i] - dLHS * QDIV[i];

                pLHS[i]  = dLHS;
                pRHS1[i] = dRHS1;
                pRHS2[i] = dRHS2;
            }

            /*** update GXGY ***/
            computeGaussSeidel(GX, pLHS, pRHS1, nIterGS, ny, nx);
            computeGaussSeidel(GY, pLHS, pRHS2, nIterGS, ny, nx);
            
            /*** Illumination term ***/
            if (dWeightLight > 1e-3) {
                /*** gradient of S ***/
                computeGradient(S, 1, SX, SY, ny, nx);
                
                /*** update PHI ***/
                #pragma omp parallel for collapse(2)
                for (int y=0; y < ny; y++) {
                    for (int x=0; x < nx; x++) {
                        float dPHIX, dPHIY;
                        PHIX[x+y*nx] = shrinkageScalar(SX[x+y*nx], dHuberLight);
                        PHIY[x+y*nx] = shrinkageScalar(SY[x+y*nx], dHuberLight);
                    }
                }
                
                computeDivergence(PHIX, PHIY, PHIDIV, ny, nx);

                #pragma omp parallel for collapse(2)
                for (int y=0; y < ny; y++) {
                    for (int x=0; x < nx; x++) {
                        float  dLHS, dRHS;
                        float  dDiv, dLambda = 0.;
                        float  dIt=0., dIx=0., dFX, dIy=0., dFY, dR=0.;
                        int idx, idxc;
                        idx = x+y*nx;

                        for (int c=0; c < nc; c++) {
                            idxc = idx + c*nx*ny;
                            dIt += I2t[idxc];
                            dIx += I2x[idxc];
                            dIy += I2y[idxc];
                            dR  +=   R[idxc];
                        }

                        dIt /= nc; dIx /= nc; dIy /= nc; dR /= nc;
                        dFX  = FX[idx]; dFY = FY[idx];
                        dDiv = PHIDIV[idx];

                        dLHS = dWeightLight * dHuberData / (dHuberLight * pLambda[idx]);
                        dRHS = dLHS * -dDiv + dIt + dIx * (dFX-FX0[idx]) + dIy * (dFY-FY0[idx]) - dR;

                        pLHS[idx]  = dLHS;
                        pRHS1[idx] = dRHS;
                    }
                }
                /*** update S ***/
                computeGaussSeidel(S, pLHS, pRHS1, nIterGS, ny, nx);
            }
        }
        // updateLambda
        if (adaptive > 0) {
            updateLambda(pLambda, pData, dBetaLambda, adaptive, param1, pWeightTV, ny, nx, nc);
        }

        if (bGT == 1 && it % 5 == 0) {
            ape = 0;
            int sum_idx=0;
            
            #pragma omp parallel for collapse(1) reduction(+:sum_idx, ape)
            for (int i=0; i < nSizeFlow; i++) {
                if (GFX[i] >= 1e5 || GFY[i] >= 1e5) {
                    continue;
                }
                sum_idx++;
                ape += sqrt( (GFX[i]-FX[i])*(GFX[i]-FX[i]) + (GFY[i]-FY[i])*(GFY[i]-FY[i]) );
            }
            ape /= (float) sum_idx;
            printf("ape: %f", it, ape);
            
            if (ape >= 10) {
                printf("divergent");
                break;
            }
        }
        printf("\n");
    }

    free(I2_warp); nMemory--;
    free(I2x); nMemory--; free(I2y); nMemory--; free(I2t); nMemory--;
    free(pData); nMemory--;
    free(R); nMemory--;

    free(S);nMemory--;
    free(FX0); nMemory--; free(FY0); nMemory--;
    free(GX); nMemory--; free(GY); nMemory--;
    free(SX); nMemory--; free(SY); nMemory--;
    free(GXx); nMemory--; free(GXy); nMemory--; free(GYx); nMemory--; free(GYy); nMemory--;

    free(PX);nMemory--; free(PY);nMemory--; free(QX);nMemory--; free(QY);nMemory--;
    free(HX);nMemory--; free(HY);nMemory--; free(pLambda);nMemory--;

    free(pLHS);nMemory--;  free(pRHS1);nMemory--; free(pRHS2);nMemory--;
    free(PDIV);nMemory--; free(QDIV);nMemory--;

    free(PHI);nMemory--; free(PHIX);nMemory--; free(PHIY);nMemory--;
     free(PHIDIV);nMemory--; free(pWeightTV);nMemory--;
    
    if (bVerbose) {
        printf("number of unncessary memory : %d\n", nMemory);
    }
    
    
}