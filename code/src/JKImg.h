/*
JKImage library for communicating with python

Rules
- The input image usually follows the order of [Channel x nRow x nCol]
- 
*/

// void interp2_bilinear(
//           float *out,
//     const float *pData,
//     const int nx,
//     const int ny,
//     const int nc,
//     )

void interp2_bicubic(
	const	float*	I,
	const	float*	u,
	const	float*	v,
			float*	out,
	const	int		nRow,
	const	int		nCol,
	const	float*	extra
	);

void computeGradient(
	const	float*  I,
	const	int     method,
            float*  I_x,
            float*  I_y,
    const   int     nRow,
    const   int     nCol
	);

// compute divergence with backward difference
void computeDivergence(
    const   float*  I1,  
    const   float*  I2,
    	    float*  out,
    const   int     nRow,
    const   int     nCol
	);

// solve: A x = b by Gauss-Seidel: A = (I - pLHS * Laplacian), b = pRHS
void computeGaussSeidel(
			float*  pData,
	const 	float*  pLHS,
	const 	float*  pRHS, 
	const	int 	nIterG,
	const	int 	ny,
	const	int 	nx
	);

float minFromArray(float* arr, int size);
float maxFromArray(float* arr, int size);


// #endif