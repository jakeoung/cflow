void motion_huberhuber(const float* I1, const float* I2, float* , float*,
    int ny, int nx, int nc, const int nIter, const int nIterInner,
    int nIterGS, int adaptive, float dLambda, float dBetaLambda, float dMinLambda, float dMaxLambda,
    float param1, float dHuberRegular, float dHuberData, float dWeightSplit, float dHuberLight,
    float dWeightLight, int, const float*, const float*, int bVerbose);