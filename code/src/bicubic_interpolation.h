void bicubic_interpolation_warp(
	const float *input,  //image to be warped
	const float *u,      //x component of the vector field
	const float *v,      //y component of the vector field
	float       *output, //warped output image with bicubic interpolation
	const int    nx,     //image width
	const int    ny,     //image height
	int          border_out//if true, put zeros outside the region
);