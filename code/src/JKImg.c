/*
JKImage library for communicating with python

Rules
- The input image usually follows the order of [Channel x ny x nx]
- 
*/

#include <math.h>
#include <stdio.h>
#include <stdbool.h>

#define MAX(a,b) (((a) >= (b)) ? (a) : (b))
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

// helper defininiton of indexing for numpy-given images of shape [C x H x W] column-wise
#define T( I, x, y, c, ny, nx ) I[ x + y*nx + c*ny*nx ]

// helper defininiton of indexing for Matlab-given images of shape [C x W x H] row-wise
// #define TMATLAB( I, x, y, c, ny, nx ) I[ y + x*ny + c*ny*nx ]

int neumann_bc(int x, int nx, bool *isOut)
{
	if (x < 0) {
		x = 0;
		*isOut = true;
	}
	else if (x >= nx) {
		x = nx - 1;
		*isOut = true;
	}
	return x;
}

//////// interpolation for optical flow
float interp_cubic_cell(float v[4], float x)
{
    return v[1] + 0.5 * x * (v[2] - v[0] +
        x * (2.0 * v[0] - 5.0 * v[1] + 4.0 * v[2] - v[3] +
        x * (3.0 * (v[1] - v[2]) + v[3] - v[0])));
}

float interp2_bicubic_at(const float*I, float xx, float yy, int ny, int nx)
{
	int sx = (xx < 0) ? -1 : 1;
	int sy = (yy < 0) ? -1 : 1;

	bool isOut[1] = { false };
	
	// neumann boundary
	int mx	= neumann_bc((int)xx - sx    , nx, isOut);
	int x   = neumann_bc((int)xx         , nx, isOut);
	int px  = neumann_bc((int)xx + sx    , nx, isOut);
	int ppx = neumann_bc((int)xx + 2 * sx, nx, isOut);

    int my  = neumann_bc((int)yy - sy    , ny, isOut);
    int y   = neumann_bc((int)yy         , ny, isOut);
    int py  = neumann_bc((int)yy + sy    , ny, isOut);
    int ppy = neumann_bc((int)yy + 2 * sy, ny, isOut);

    if (*isOut) {
        return -1.0;
    }
    
    float p11 = I[mx + my * nx];
    float p12 = I[x + my * nx];
    float p13 = I[px + my * nx];
    float p14 = I[ppx + my * nx];

    float p21 = I[mx + y * nx];
    float p22 = I[x + y * nx];
    float p23 = I[px + y * nx];
    float p24 = I[ppx + y * nx];

    float p31 = I[mx + py * nx];
    float p32 = I[x + py * nx];
    float p33 = I[px + py * nx];
    float p34 = I[ppx + py * nx];

    float p41 = I[mx + ppy * nx];
    float p42 = I[x + ppy * nx];
    float p43 = I[px + ppy * nx];
    float p44 = I[ppx + ppy * nx];

    float p[4][4] = {
        { p11, p21, p31, p41 },
        { p12, p22, p32, p42 },
        { p13, p23, p33, p43 },
        { p14, p24, p34, p44 }
    };

    float v[4];
    v[0] = interp_cubic_cell(p[0], yy - y);
    v[1] = interp_cubic_cell(p[1], yy - y);
    v[2] = interp_cubic_cell(p[2], yy - y);
    v[3] = interp_cubic_cell(p[3], yy - y);

    return interp_cubic_cell(v, xx - x);
}

// Original author: Javier Sánchez Pérez <jsanchez@dis.ulpgc.es>
void interp2_bicubic(
	const	float*	I,
	const	float*	u,
	const	float*	v,
			float*	out,
	const	int		ny,
	const	int		nx,
	const	float*	extra
)
{
    #pragma omp parallel for collapse(2)
	for (int y = 0; y < ny; y++) {
		for (int x = 0; x < nx; x++) {
			int idx = x + y * nx;
			float xx = x + u[idx];
			float yy = y + v[idx];

            out[idx] = interp2_bicubic_at(I, xx, yy, ny, nx);
            if (out[idx] < 0) {
                out[idx] = extra[idx];
            }
		}
	}
}




void computeGradient(
	const	float*  I,
	const	int     method,
            float*  I_x,
            float*  I_y,
    const   int     ny,
    const   int     nx
	)
{
	// forward difference
	if (method == 1) {
        #pragma omp parallel for collapse(2)
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                // w.r.t. x
                float valNow, valNext;
                valNow = I[x + y * nx];
                
                if (x+1 == nx)
                    valNext = I[x+y*nx];
                else
                    valNext = I[x+1+y*nx];
                
                I_x[x+y*nx] = valNext - valNow;

                // w.r.t. y
                if (y+1 == ny)
                    valNext = I[x+y*nx];
                else
                    valNext = I[x + (y+1) * nx];

                I_y[x+y*nx] = valNext - valNow;
            }
        }
	}
	// centered difference
	else if (method == 0) {
		#pragma omp parallel for collapse(2)
        for (int y=0; y < ny; y++) {
            for (int x=0; x < nx; x++) {
                // w.r.t. x
                float valxp, valxm, valyp, valym;
                int xp, xm, yp, ym;

                // xp: xplus, xm: xminus
                xp = MIN(x+1, nx-1);
                xm = MAX(x-1, 0);
                yp = MIN(y+1, ny-1);
                ym = MAX(y-1, 0);

                valxp = I[xp + y * nx];
                valxm = I[xm + y * nx];
                valyp = I[x + yp * nx];
                valym = I[x + ym * nx];

                I_x[x+y*nx] = 0.5 * valxp - 0.5 * valxm;
                I_y[x+y*nx] = 0.5 * valyp - 0.5 * valym;
            }
        }
	}
}

// compute divergence with backward difference
void computeDivergence(
    const   float*  I1,  
    const   float*  I2,
    	    float*  out,
    const   int     ny,
    const   int     nx
	)
{
	// backward difference
	#pragma omp parallel for collapse(2)
    for (int y=0; y < ny; y++) {
        for (int x=0; x < nx; x++) {
            float val1, val1xm, val2, val2ym, dx, dy;
            int xm, ym, idx;

            idx = x + y * nx;

            // xm: xminus
            xm = MAX(x-1, 0);
            ym = MAX(y-1, 0);

            val1 = I1[x+y*nx];
            val2 = I2[x+y*nx];

            val1xm = I1[xm+y*nx];
            val2ym = I2[x+ym*nx];

            if (x == 0) {
                val1xm = 0;
            } else if (x+1 == nx) {
                val1 = 0;
            }

            if (y == 0) {
                val2ym = 0;
            } else if (y+1 == ny) {
                val2 = 0;
            }

            dx = val1 - val1xm;
            dy = val2 - val2ym;

            out[x+y*nx] = dx + dy;
        }
    }
}

// solve: A x = b by Gauss-Seidel: A = (I - pLHS * Laplacian), b = pRHS
void computeGaussSeidel(float* pData, const float* pLHS, const float* pRHS, const int nIterGS, const int ny, const int nx)
{
    float  hh = 0.25;
    
    for(int i = 0; i < nIterGS; i++)
    {
        #pragma omp parallel for collapse(2)
        for(int y = 0; y < ny; y++)
        {
            for(int x = 0; x < nx; x++)
            {
                float dX2, dA, dB, dAxp, dAxm, dAyp, dAym, dXxp, dXxm, dXyp, dXym;
                int xm, xp, ym, yp, idx;
                
                idx = x + y * nx;
                
                dA  = pLHS[idx];
                dB  = pRHS[idx];

                // getNeighbour4
                xm = MAX(x-1, 0);
                xp = MIN(x+1, nx-1);
                
                ym = MAX(y-1, 0);
                yp = MIN(y+1, ny-1);

                dAxp = pLHS[xp + y * nx];
                dAxm = pLHS[xm + y * nx];
                dAyp = pLHS[x + yp * nx];
                dAym = pLHS[x + ym * nx];
                
                dXxp = pData[xp + y * nx];
                dXxm = pData[xm + y * nx];
                dXyp = pData[x + yp * nx];
                dXym = pData[x + ym * nx];

                dX2 = hh * dB + (dAxp * dXxp + dAxm * dXxm + dAyp * dXyp + dAym * dXym);
                dX2 = dX2 / (hh + 4.0 * dA);
                
                pData[idx] = dX2;
            }
        }
    }
}

float maxFromArray(float* arr, int size)
{
    float dMax = arr[0];
    for (int i=1; i < size; i++) {
        if (arr[i] > dMax) {
            dMax = arr[i];
        }
    }
    return dMax;
}

float minFromArray(float* arr, int size)
{
    float dMin = arr[0];
    for (int i=1; i < size; i++) {
        if (arr[i] < dMin) {
            dMin = arr[i];
        }
    }
    return dMin;
}

int neumann_bc_x(int c, int r, int ny, int nx, bool *isOut, const int* R)
{
    if (c < 0 ) {
        c = 0;
        *isOut = true;
    }
    else if (c > 0 && r > 0 && r < ny && R[ c-1 + r*nx] == 0 ) {
        //c = 0;
        *isOut = true;
    }
    else if (c >= nx) {
        c = nx - 1;
        *isOut = true;
    }
    else if (c + 1 < nx && r > 0 && r < ny && R[ c+1 + r*nx] == 0) {
        //c = c;
        *isOut = true;
    }
    return c;
}

// assume that c is within region
int neumann_bc_y(int c, int r, int ny, int nx, bool *isOut, const int* R)
{
    if (r < 0) {
        r = 0;
        *isOut = true;
    }
    else if (r > 0 && R[ c + (r-1)*nx] == 0) {
        //r = 0;
        *isOut = true;
    }
    else if (r >= ny) {
        r = ny - 1;
        *isOut = true;
    }
    else if (r+1 < ny &&  R[ c + (r+1)*nx] == 0) {
        //c = c;
        *isOut = true;
    }
    return r;
}
#define ZOOM_SIGMA_ZERO 0.6

/**
  *
  * Compute the size of a zoomed image from the zoom factor
  *
**/
void zoom_size(
    int nx,             //width of the orignal image
    int ny,             //height of the orignal image
    int &nxx,           //width of the zoomed image
    int &nyy,           //height of the zoomed image
    float factor = 0.5  //zoom factor between 0 and 1
)
{
    nxx = (int)((float) nx * factor + 0.5);
    nyy = (int)((float) ny * factor + 0.5);
}

/**
  *
  * Function to downsample the image
  *
**/
void zoom_out(
    const float *I,          //input image
    float *Iout,             //output image
    const int nx,            //image width
    const int ny,            //image height
    const float factor = 0.5 //zoom factor between 0 and 1
)
{
    int nxx, nyy;

    float *Is = new float[nx * ny];

    for(int i = 0; i < nx * ny; i++)
	Is[i] = I[i];

    //calculate the size of the zoomed image
    zoom_size(nx, ny, nxx, nyy, factor);

    //compute the Gaussian sigma for smoothing
    const float sigma = ZOOM_SIGMA_ZERO * sqrt(1.0/(factor*factor) - 1.0);

    //pre-smooth the image
    gaussian(Is, nx, ny, sigma);

    // re-sample the image using bicubic interpolation
	for (int i1 = 0; i1 < nyy; i1++)

	    for (int j1 = 0; j1 < nxx; j1++)
	    {
		const float i2  = (float) i1 / factor;
		const float j2  = (float) j1 / factor;

		Iout[i1 * nxx + j1] = bicubic_interpolation(Is, j2, i2, nx, ny);
	    }

    delete []Is;
}


void upsample(float *out)
{

}

/**
  *
  * Function to upsample the image
  *
**/
void zoom_in(
    const float *I, //input image
    float *Iout,    //output image
    int nx,         //width of the original image
    int ny,         //height of the original image
    int nxx,        //width of the zoomed image
    int nyy         //height of the zoomed image
)
{
    // compute the zoom factor
    const float factorx = ((float)nxx / nx);
    const float factory = ((float)nyy / ny);

    // re-sample the image using bicubic interpolation
	for (int i1 = 0; i1 < nyy; i1++)
	    for (int j1 = 0; j1 < nxx; j1++)
	    {
		float i2 =  (float) i1 / factory;
		float j2 =  (float) j1 / factorx;

		Iout[i1 * nxx + j1] = bicubic_interpolation(I, j2, i2, nx, ny);
	    }
}