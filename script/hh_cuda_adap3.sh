#!/bin/bash

niter="500"
pyramid="1"
beta="1"

for dHuberData in 0.001 0.01 0.05 0.1; do
for dHuberRegular in 0.3; do
for dWeightSplit in 0.1 0.01 0.5 1; do
for dHuberLight in 0.01 0.1 0.2; do
for dWeightLight in 20; do

for seq in 0 1 2 3 4 5 6 7; do
for lambd in 0.00001 0.001 0.01 0.1 0.15 0.2 0.3 0.5 1 10 100 1000; do
for param1 in 0.1 1 10; do

    CUDA_DEVICE=$1 python3 ../code_cu/main.py -a -3 --seq $seq --lambd $lambd -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight --dWeightSplit $dWeightSplit --dHuberRegular $dHuberRegular --dHuberData $dHuberData --dBeta $beta --param1 $param1

done
done
done

done
done
done
done
done
