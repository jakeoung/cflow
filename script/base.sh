#!/bin/bash

if [ $1 = 0 ]; then
    model="hs"
elif [ $1 = 1 ]; then
    model="tvl1"
elif [ $1 = 2 ]; then
    model="hl1"
fi

niter="2000"
pyramid="3"
tau="0.25"

if [ $2 = 0 ]; then
    for seq in 0 1; do
        for lambd in 0.1 0.25 1 4 16 64 256 1024; do
            python3 ../baseline/main.py -m $model --seq $seq --lambd $lambd -n $niter -p $pyramid -t $tau
        done
    done

elif [ $2 = 1 ]; then
    for seq in 2 3; do
        for lambd in 0.1 0.25 1 4 16 64 256 1024; do
            python3 ../baseline/main.py -m $model --seq $seq --lambd $lambd -n $niter -p $pyramid -t $tau
        done
    done

elif [ $2 = 2 ]; then
    for seq in 4 5; do
        for lambd in 0.1 0.25 1 4 16 64 256 1024; do
            python3 ../baseline/main.py -m $model --seq $seq --lambd $lambd -n $niter -p $pyramid -t $tau
        done
    done

elif [ $2 = 3 ]; then
    for seq in 6 7; do
        for lambd in 0.1 0.25 1 4 16 64 256 1024; do
            python3 ../baseline/main.py -m $model --seq $seq --lambd $lambd -n $niter -p $pyramid -t $tau
        done
    done
fi
