#!/bin/bash

niter="2000"
pyramid="2"
beta="1"

for seq in 0 1 2 3 4 5 6 7; do
for dHuberData in 0.001 0.01 0.05 0.1; do
for dHuberRegular in 0.3; do
for dWeightSplit in 0.1 0.01 0.5 1; do
for dHuberLight in 0.01 0.1 0.2; do
for dWeightLight in 20 0.000001; do

for lambd in 0.0001 0.001 0.01 0.05 0.1 0.25 0.5 0.8 0.12 0.25 0.5 0.75 1 2 4 8 16 32 64 128 256 512 1024; do

    CUDA_DEVICE=$1 python3 ../code_cu/main.py -a -1 --lambd $lambd --seq $seq -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight --dWeightSplit $dWeightSplit --dHuberRegular $dHuberRegular --dHuberData $dHuberData

done
done

done
done
done
done
done

