#!/bin/bash

export OMP_NUM_THREADS=4

niter="2000"
pyramid="3"

for dHuberData in 0.0001 0.001 0.005; do
for dHuberRegular in 0.1 0.3 0.7; do
for dWeightSplit in 0.005 0.1 0.01; do
for dHuberLight in 0.01 0.1 0.2; do
for dWeightLight in 0 0.5 10 20; do

for lambd in 0.5 0.7 0.75 0.8 0.82 0.86 0.9 0.92 0.96 0.98 0.99; do
    python3 ../code/main.py --seq $1 --lambd $lambd -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight
done

done
done
done
done
done