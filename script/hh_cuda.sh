#!/bin/bash

niter="500"
pyramid=$2
alphaMode="3"

for seq in 3 4 5 6 7 0 1 2; do
for dHuberData in 0.01; do
for dHuberRegular in 0.5; do
for dWeightSplit in 0.1; do
for dHuberLight in 0.4; do
for dWeightLight in 20; do

for alpha in 1 2 3 4 5 6 7 8 9 10; do
for alphaRatio in 0.95; do

#for lambd in 0.15 0.925 0.935 0.945 0.955 0.965 0.975 0.985 0.99 0.993 0.995 0.997; do
for lambd in 0.8 0.9 0.93 0.95 0.97 0.98 0.99 0.995; do
    CUDA_DEVICE=$1 python3 ../code_cu/main.py --seq $seq --lambd $lambd -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight --dHuberData $dHuberData --dHuberRegular $dHuberRegular --dWeightSplit $dWeightSplit --alpha $alpha --alphaRatio $alphaRatio --alphaMode $alphaMode -vv
done
done 
done
done
done
done
done
done
done
