#!/bin/bash

export OMP_NUM_THREADS=4

niter="2000"
pyramid="3"

for lambd in 0.3 0.5 0.6 0.7 0.75 0.8 0.83 0.86 0.88 0.9 0.92 0.94 0.96 0.98 0.99; do
    python3 ../code/main.py --seq $1 --lambd $lambd -n $niter -p $pyramid
done
