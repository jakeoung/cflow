#!/bin/bash

export OMP_NUM_THREADS=4

niter="500"
pyramid="3"

for lambd in 0.0001 0.001 0.01 0.05 0.1 0.5 1 2 4 8 16 32 64; do
for theta in 0.01 0.05 0.1 0.5 1 10; do
    python3 ../zach2007/main.py --seq $1 --lambd $lambd --theta $theta -n $niter -p $pyramid -v
done
done