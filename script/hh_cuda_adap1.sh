#!/bin/bash

niter="1000"
pyramid="3"

for dHuberData in 0.01 0.008; do
for dHuberRegular in 0.3; do
for dWeightSplit in 0.1; do
for dHuberLight in 0.3; do
for dWeightLight in 0 15; do

#for beta in 0.5 0.9 1 10 50; do
for beta in 0.8 1 4 16 64; do
for seq in 1 2 5 6 7; do
for param1 in 0.005 0.001 0.01 0.02 0.03 0.04 0.05 0.06; do

    CUDA_DEVICE=$1 python3 ../code_cu/main.py -a 1 --seq $seq -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight --dWeightSplit $dWeightSplit --dHuberRegular $dHuberRegular --dHuberData $dHuberData --dBeta $beta --param1 $param1 --alpha 0.5 --alphaRatio 1.2 -v

done
done
done

done
done
done
done
done

