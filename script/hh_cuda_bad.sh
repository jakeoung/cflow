#!/bin/bash

niter="1000"
pyramid=$2

for seq in 1 5 6 7; do
for dHuberData in 0.008; do
for dHuberRegular in 0.5; do
for dWeightSplit in 0.1; do
for dHuberLight in 0.3; do
for dWeightLight in 25; do

for alpha in 0.5; do
for alphaRatio in 1.05; do

for lambd in 0.905 0.915 0.925 0.935 0.945 0.955 0.965 0.975 0.985 0.99 0.995; do
    CUDA_DEVICE=$1 python3 ../code_cu/main.py --seq $seq --lambd $lambd -n $niter -p $pyramid --dHuberLight $dHuberLight --dWeightLight $dWeightLight --dHuberData $dHuberData --dHuberRegular $dHuberRegular --dWeightSplit $dWeightSplit --alpha $alpha --alphaRatio $alphaRatio
done
done 
done
done
done
done
done
done
done
