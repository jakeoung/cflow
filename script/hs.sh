#!/bin/bash

export OMP_NUM_THREADS=4

niter="1000"
pyramid="3"
niterinner="500"

for alpha in 0.001 0.01 0.05 0.1 0.25 0.5 1 2 4 8 16 32 48 64; do
    python3 ../hs/main.py --seq $1 --niterinner 500 -n $niter -p $pyramid
done