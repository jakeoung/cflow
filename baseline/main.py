from motion import *
import numpy as np
import os
import time
import math

from PIL import Image
from scipy import misc
from scipy import ndimage

import argparse

##############################
# parsing arguments to be used in output file name
##############################
parser = argparse.ArgumentParser(description='optical flow')
parser.add_argument('-m','--model', type=str, default='hl1', help='motion: hs, tvl1, hl1')
parser.add_argument('--seq', type=int, default=4, help='sequence number in middlebury(0~7)')
parser.add_argument('-n', '--niter', type=int, default='50', help='iteration number')
parser.add_argument('-p', '--pyramid', type=int, default='1', help='number of scales in pyramid')

parser.add_argument('-t', '--tau', type=float, default='0.25', help='step size in Chambolle-Pock')
parser.add_argument('--lambd', type=float, default='0.95', help='constant lambda')

args, unparsed = parser.parse_known_args()
fResult = time.strftime('%m%d_')
dic = vars(args)
for key in dic:
    fResult += '--' + str(key) + '_' + str(dic[key]) + '_'

##############################
# parsing other arguments
##############################
parser = argparse.ArgumentParser(parents=[parser], conflict_handler='resolve')
parser.add_argument('--root', type=str, default='../', help='work folder')
parser.add_argument('--dResult', type=str, default='../result/base/')
parser.add_argument('--gpu', type=int, default='-1')
parser.add_argument('-v','--verbose', action='store_true', default=0, help='verbose mode')

args = parser.parse_args(namespace=args)
fResult_ = os.path.join(args.dResult, fResult)
print(args)
print(fResult_)

##############################
# input, pre-processing
##############################
seq_list = ['Dimetrodon','Grove2','Grove3','Hydrangea',
            'RubberWhale','Urban2','Urban3','Venus']

# dir_data= 'data/other-data-two/'
dir_data= 'data/other-data-two/'

im1ori = Image.open(os.path.join(args.root,
    dir_data+seq_list[args.seq]+'/frame10.png'))

im2ori = Image.open(os.path.join(args.root,
    dir_data+seq_list[args.seq]+'/frame11.png'))

gt = flowlib.read_flow(os.path.join(args.root,
    'data/other-gt-flow/'+seq_list[args.seq]+'/flow10.flo'))

if isinstance(gt, int) == False:
    gt1 = gt[:,:,0].astype(dtype=np.float32)
    gt2 = gt[:,:,1].astype(dtype=np.float32)

im1_ = np.array(im1ori)
im2_ = np.array(im2ori)

im1 = np.atleast_3d(im1_)
im2 = np.atleast_3d(im2_)
#
# im1 = im1.transpose(2,0,1)
# im2 = im2.transpose(2,0,1)
#
# im1 = (im1 - im1.min()) / (im1.max() - im1.min())
# im2 = (im2 - im2.min()) / (im2.max() - im2.min())
##############################
# model selection
##############################
args.cuda = False
if args.gpu > -1:
    args.cuda = True

motion = Motion(im1, im2, model=args.model, niter=args.niter,
                    lambd=args.lambd, tau=args.tau, gt=gt, cuda=args.cuda)

# motion = MotionNumba(im1, im2, model=args.model, niter=args.niter,
#                     lambd=args.lambd, tau=args.tau, gt=gt, drv=args.drv)


motion.do_pyramid(args.pyramid)

FX = motion.FX
FY = motion.FY

##############################
# write output
##############################
if isinstance(gt, int) == False:
    ape, aae = flowlib.flow_error(gt[:,:,0], gt[:,:,1], FX, FY)
    print('ape: %f, aae: %f, max_FX: %f' % (ape, aae, FX.max()))

if args.verbose:
    flow_img = flowlib.flow_to_image(FX, FY)
    im = Image.fromarray(flow_img).show()

if os.path.exists(args.dResult):
    np.save( fResult_, [FX, FY, ape, aae] )